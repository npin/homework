package hw1;

public class ParkingMeterTester {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ParkingMeter pm0 = new ParkingMeter(6, 15, 45, 500, 10);
		//pm0.insertNickels(2);
		System.out.println(pm0.getTotalCoins());
		System.out.println(pm0.getCents());
		System.out.println(pm0.getMinutesRemaining());
		//pm0.simulateTime(5);
		pm0.insertNickels(8);
		//pm0.simulateTime(10);	
		System.out.println(pm0.getTotalCoins());
		System.out.println(pm0.getCents());
		System.out.println(pm0.getMinutesRemaining());
		pm0.insertQuarters(5);
		System.out.println(pm0.getTotalCoins());
		System.out.println(pm0.getCents());
		System.out.println(pm0.getMinutesRemaining());
		System.out.println(pm0);
		System.out.println(pm0.getHourMinuteString());
		pm0.emptyCoins();
		pm0.insertQuarters(20);
		System.out.println(pm0.getTotalCoins());
		System.out.println(pm0.getCents());
		System.out.println(pm0.getMinutesRemaining());
		System.out.println(pm0);
		System.out.println(pm0.getHourMinuteString());
		System.out.println(pm0.getDollarString());
	}

}
