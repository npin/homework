package mini1;

public class TV 
{
	public static final double VOLUME_INCREMENT = 0.07;
	private int currentChannel; 
	private double currentVolume;
	private int channelMax; //Setting the max of the channel numbers
	private int channelMin;
	private int previousChannel;

	/*
	** Constructor TV Constructs a new TV with available channels 0 through givenChannelMax - 1.
	*/
	public TV(int givenChannelMax)	           
	{
	channelMin = 0;
	channelMax = givenChannelMax;
	currentChannel = 0;
	currentVolume = 0.50;
	previousChannel = 0;
	}
	
	/*
	** Method channelDown Changes the channel down by 1, wrapping around to channelMax - 1 if the current channel is zero.
	*/
	public void channelDown() 
	{
		previousChannel = currentChannel;
		if (currentChannel == channelMin)
		{
		currentChannel = channelMax-1;
		}
		else
		{
		currentChannel = currentChannel-1;
		}
		
	}
	        
	/*
	** Method channelUp Changes the channel up by 1, wrapping around to zero if the current channel is channelMax - 1.
	*/
	public void channelUp() 
	{
		previousChannel = currentChannel;
		if (currentChannel == channelMax-1)
		{
		currentChannel = channelMin;
		}
		else
		{
		currentChannel = currentChannel+1;
		}
	}

	/*
	** Method getChannel()Returns the current channel for this TV.
	*/
	public int getChannel() 
	{
		return currentChannel;
	}

	/*
	** Method getVolume Returns the current volume for this TV.
	*/
	public double getVolume() 
	{
		return currentVolume;
	}

	/*
	** Method goToPreviousChannel Sets the current channel to the most recent previous channel.
	*/
	public void goToPreviousChannel() 
	{
		
		if ((previousChannel == 0) && (currentChannel == 0))
		{
			System.out.println("ERROR"); //Alert if the channel has not been changed
		}
		else 
		{
			int tempChannel = currentChannel; // Swap the previous channel and the current channel
			currentChannel = previousChannel;
			previousChannel = tempChannel;
		}
	}

	/*
	** Method setChannel(int number) Sets the channel to the given channel number
	*/
	public void setChannel(int number) 
	{
		if (channelMax < number)
		{
			previousChannel = currentChannel;
			currentChannel = Math.min(number-1, channelMax-1);
		}
		else if (channelMin > number)
		{
			previousChannel = currentChannel;
			currentChannel = Math.max(number-1, channelMin);
		}
		else
		{
			previousChannel = currentChannel;
			currentChannel = number;
		}
	}

	/*
	** Method volumDown Lowers the volume by VOLUME_INCREMENT, but not below 0.0.
	*/
	public void volumeDown() 
	{
		currentVolume = Math.max(currentVolume - VOLUME_INCREMENT, 0.0);
	}

	/*
	** Method volumeUp Raises the volume by VOLUME_INCREMENT, but not above 1.0.
	*/
	public void volumeUp() 
	{
		currentVolume = Math.min(currentVolume + VOLUME_INCREMENT, 1.0);
	}


}

