package hw2;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
/**
 * Unit test for HideableChar Class
 * @author Siyu Lin
 *
 */
public class HideableCharTest {
	
	//Initial test
	
	//Test the letters
	
	//Test the lower case letter
	//Test the hidden status
	@Test
	public void testLowerLetterHidden(){
		String msg = "After constructing a HideableChar with character A, isHidden should return true";
		HideableChar lwLetter = new HideableChar('a');
		assertEquals(msg, true, lwLetter.isHidden());
	}
	//Test the display status
	@Test
	public void testLowerLetterDispay(){
		String msg = "The lower letter a should not be displayed";
		HideableChar lwLetter = new HideableChar('a');
		assertEquals(msg, null, lwLetter.getDisplayedChar());
	}
	
	//Test the upper case letter
	//Test the hidden status
	@Test
	public void testUpperLetterHidden(){
		String msg = "The letter A should be hidden";
		HideableChar upLetter = new HideableChar('A');
		assertEquals(msg, true, upLetter.isHidden());
	}
	//Test the display status
	@Test
	public void testUpperLetterDispay(){
		String msg = "The letter A should not be displayed";
		HideableChar upLetter = new HideableChar('A');
		assertEquals(msg, null, upLetter.getDisplayedChar());
	}
	
	//Test the non-alphabetic characters
	
	//Test the space
	//Test the hidden status
	@Test
	public void testSpaceHiddenAndDisplay(){
		String msg = "The space should not be hidden and be displayed  ";
		HideableChar space = new HideableChar(' ');
		assertEquals(msg, false, space.isHidden());
		assertEquals(msg, " ", space.getDisplayedChar());
	}
	
	//Test the number
	//Test the hidden status
	@Test
	public void testNumberHiddenAndDisplay(){
		String msg = "The number 1 should not be hidden and be displayed as 1";
		HideableChar num = new HideableChar('1');
		assertEquals(msg, false, num.isHidden());
		assertEquals(msg, "1", num.getDisplayedChar());
	}
	
	//Test the exclamation mark
	//Test the hidden status
	@Test
	public void testExmarkHiddenAndDisplay(){
		String msg = "The exclamation mark ! should not be hidden and be displayed as ! ";
		HideableChar exmark = new HideableChar('!');
		assertEquals(msg, false, exmark.isHidden());
		assertEquals(msg, "!", exmark.getDisplayedChar());
	}

	//Method tests
	
	//Unhide the lower case letter
	//Test the hidden status
	@Test
	public void testLowerLetterHideAndGetDisplay(){
		String msg = "After unhiding, the initially hidden lower letter a should not be hidden, displayed as a";
		HideableChar lwLetter = new HideableChar('a');
		assertEquals(msg, true, lwLetter.isHidden());
		lwLetter.unHide();
		assertEquals(msg, false, lwLetter.isHidden());
		assertEquals(msg, "a", lwLetter.getDisplayedChar());
	}
	
	//Unhide the upper case letter
	//Test the hidden status and display
	@Test
	public void testUpperLetterHideAndGetDisplay(){
		String msg = "After unhiding, the upper letter A should not be hidden and be displayed as A ";
		HideableChar upLetter = new HideableChar('A');
		assertEquals(msg, true, upLetter.isHidden());
		upLetter.unHide();
		assertEquals(msg, false, upLetter.isHidden());
		assertEquals(msg, "A", upLetter.getDisplayedChar());
	}
	
	//Hide the dash
	//Test the hidden status and display status
	@Test
	public void testDashHideAndGetDisplay() {
		String msg = "The dash - should intially be not hidden and after hiding be hidden and be displayed as ";
		HideableChar dash = new HideableChar('-');
		assertEquals(msg, false, dash.isHidden());
		dash.hide();
		assertEquals(msg, true, dash.isHidden());
		assertEquals(msg, null, dash.getDisplayedChar());
	}
		
	
	//Get the character whether hidden or not
	//Get the hidden letter
	@Test
	public void testGetLetter(){
		String msg = "After calling getHiddenChar, the hidde upper letter A should be displayed ";
		HideableChar upLetter = new HideableChar('A');
		assertEquals(msg, true, upLetter.isHidden());
		assertEquals(msg, "A", upLetter.getHiddenChar());
	}
	//Get the unhidden letter
	@Test
	public void testGetLetterAfterUnhide(){
		String msg = "After calling getHiddenChar and unhide, the unhidden upper letter A should be not be hidden and be displayed ";
		HideableChar upLetter = new HideableChar('A');
		upLetter.unHide();
		assertEquals(msg, false, upLetter.isHidden());
		assertEquals(msg, "A", upLetter.getHiddenChar());
	}
	//Get the unhidden non-alphabetic characters 
	@Test
	public void testGetNonLetter(){
		String msg = "After calling getHiddenChar, the unhidden number 1 should be displayed ";
		HideableChar num = new HideableChar('1');
		assertEquals(msg, false, num.isHidden());
		assertEquals(msg, "1", num.getHiddenChar());
	}
	//Get the hidden non-alphabetic characters
	@Test
	public void testGetNonLetterAfterHide(){
		String msg = "After calling getHiddenChar and hide, the hidden number 1 should be hidden and displayed as 1";
		HideableChar num = new HideableChar('1');
		assertEquals(msg, false, num.isHidden());
		num.hide();
		assertEquals(msg, true, num.isHidden());
		assertEquals(msg, "1", num.getHiddenChar());
	}
	
	//Test the matches method Hidd
	
	//Test the letters
	@Test
	public void testLowerLetterMatchCaseSensitive(){
		String msg = "With A, the lower case a does not match";
		HideableChar lwLetter = new HideableChar('a');
		assertEquals(msg, false, lwLetter.matches('A'));
	}
	@Test
	public void testLowerLetterMatchCaseSensitive2(){
		String msg = "With a, the lower case a matches ";
		HideableChar lwLetter = new HideableChar('a');
		assertEquals(msg, true, lwLetter.matches('a'));
	}
	@Test
	public void testUpperLetterMatchCaseSensitive(){
		String msg = "With a, the upper case A does not match ";
		HideableChar upLetter = new HideableChar('A');
		assertEquals(msg, false, upLetter.matches('a'));
	}
	@Test
	public void testUpperLetterMatchCaseSensitive2(){
		String msg = "With A, the upper case A matches ";
		HideableChar upLetter = new HideableChar('A');
		assertEquals(msg, true, upLetter.matches('A'));
	}
	//Test the numbers
	@Test
	public void testNumbersMatch1(){
		String msg = "With letter a, the number 1 does not match ";
		HideableChar upLetter = new HideableChar('1');
		assertEquals(msg, false, upLetter.matches('a'));
	}
	@Test
	public void testNumbersMatch2(){
		String msg = "With number 1, the number 1 does match";
		HideableChar num = new HideableChar('1');
		assertEquals(msg, true, num.matches('1'));
	}
	//Test the space
	@Test
	public void testSpaceMatch1(){
		String msg = "With letter a, the space does not match";
		HideableChar space = new HideableChar(' ');
		assertEquals(msg, false, space.matches('a'));
	}
	@Test
	public void testSpaceMatch2(){
		String msg = "With space, the space does match";
		HideableChar space = new HideableChar(' ');
		assertEquals(msg, true, space.matches(' '));
	}
	//Test the exclamation mark
	@Test
	public void testExmarkMatch1(){
		String msg = "With letter a, the exclamation mark ! does not match";
		HideableChar exmark = new HideableChar('!');
		assertEquals(msg, false, exmark.matches('a'));
	}
	@Test
	public void testExmarkMatch2(){
		String msg = "With !, the exclamation mark ! does match";
		HideableChar exmark = new HideableChar('!');
		assertEquals(msg, true, exmark.matches('!'));
	}
	
}
