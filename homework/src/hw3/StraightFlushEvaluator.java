package hw3;

import java.util.ArrayList;

import api.Card;
import api.Hand;
import api.IEvaluator;
import api.Suit;

/**
 * Evaluator for a hand consisting of a "straight" in which the card ranks are
 * consecutive numbers AND the cards all have the same suit. The number of
 * required cards is equal to the hand size. An ace (card of rank 1) may be
 * treated as the highest possible card or as the lowest (not both) To evaluate
 * a straight containing an ace it is necessary to know what the highest card
 * rank will be in a given game; therefore, this value must be specified when
 * the evaluator is constructed. In a hand created by this evaluator the cards
 * are listed in descending order with high card first, e.g. [10 9 8 7 6] or [A
 * K Q J 10], with one exception: In case of an ace-low straight, the ace must
 * appear last, as in [5 4 3 2 A]
 * 
 * The name of this evaluator is "Straight Flush".
 */
public class StraightFlushEvaluator extends AbstractEvaluator {
	/**
	 * Constructs the evaluator. Note that the maximum rank of the cards to be
	 * used must be specified in order to correctly evaluate a straight with ace
	 * high.
	 * 
	 * @param ranking
	 *            ranking of this hand
	 * @param handSize
	 *            number of cards in a hand
	 * @param maxCardRank
	 *            largest rank of any card to be used
	 */
	private int maxCardRank;
	
	public StraightFlushEvaluator(int ranking, int handSize, int maxCardRank) {
		super();
		super.ranking = ranking;
		super.handSize = handSize;
		super.numMainCards = handSize;
		super.name = "Straight Flush";
		this.maxCardRank = maxCardRank;
	}

	@Override
	public boolean canSatisfy(Card[] mainCards) {
		IEvaluator straightE  = new StraightEvaluator(this.ranking,this.handSize,this.maxCardRank);
		
		if(!straightE.canSatisfy(mainCards))	{
			return false;
		}
		else{
			return haveSameSuits(mainCards);
		}
	}

	@Override
	public String getName() {
		return super.getName();
	}

	@Override
	public int getRanking() {
		return super.getRanking();
	}

	@Override
	public int cardsRequired() {
		return super.cardsRequired();
	}

	@Override
	public int handSize() {
		return super.cardsRequired();
	}

	@Override
	public boolean canSubsetSatisfy(Card[] allCards) {
		return super.canSubsetSatisfy(allCards);
	}

	@Override
	public Hand createHand(Card[] allCards, int[] subset) {
		IEvaluator straightE = new StraightEvaluator(ranking, handSize, maxCardRank);
		Hand hand = straightE.createHand(allCards, subset);
		
		if(canSatisfy(hand.getMainCards()))
		{
			return new Hand(hand.getMainCards(), hand.getSideCards(), this);
		}
		else{
			return null;
		}
	}

	@Override
	public Hand getBestHand(Card[] allCards) {
		return super.getBestHand(allCards);
	}
	
	/**
	 * Check if all main cards have same suits 
	 * @param mainCards
	 * @return true if all mainCards have same suits
	 */
	private boolean haveSameSuits(Card[] mainCards){
		ArrayList<Card> mainCardsList = new ArrayList<Card>();
		super.cardsListGen(mainCards, mainCardsList);
		
		for (int i = 0; i < mainCards.length-1; i ++){
			boolean isSuitsEual = mainCardsList.get(i).getSuit().compareTo(mainCardsList.get(i + 1).getSuit()) == 0;
			if (!isSuitsEual){
				return false;
			}
		}
		
		return true;
	}
}
