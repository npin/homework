package hw3;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.util.Arrays;

import org.junit.Test;

import api.Card;
import api.Hand;
import api.IEvaluator;
import api.Suit;

public class CardsTest {

	public static void main(String[] args) {
		// Card c1 = new Card(2, Suit.CLUBS);
		// Card c2 = new Card(3, Suit.CLUBS);
		//
		// // is c1 before c2?
		// boolean result = c1.compareTo(c2) > 0;
		// System.out.println(result);
		//
		Card[] AsAdKh7c5d = { new Card(1, Suit.SPADES),
				new Card(1, Suit.DIAMONDS), new Card(13, Suit.HEARTS),
				new Card(7, Suit.CLUBS), new Card(5, Suit.DIAMONDS) };
		Card[] AsAdKh5c5d = { new Card(1, Suit.SPADES),
				new Card(1, Suit.DIAMONDS), new Card(13, Suit.HEARTS),
				new Card(5, Suit.CLUBS), new Card(5, Suit.DIAMONDS) };
		Card[] Ah10c9s8d7c = { new Card(1, Suit.HEARTS),
				new Card(10, Suit.CLUBS), new Card(9, Suit.SPADES),
				new Card(8, Suit.DIAMONDS), new Card(7, Suit.CLUBS) };
		Card[] Ah9s9h9d7c = { new Card(1, Suit.HEARTS),
				new Card(9, Suit.SPADES), new Card(9, Suit.HEARTS),
				new Card(9, Suit.DIAMONDS), new Card(7, Suit.CLUBS) };
		Card[] Ah9s9h9d9c = { new Card(1, Suit.HEARTS),
				new Card(9, Suit.SPADES), new Card(9, Suit.HEARTS),
				new Card(9, Suit.DIAMONDS), new Card(9, Suit.CLUBS) };
		Card[] tend10c10h5s5c = { new Card(10, Suit.HEARTS),
				new Card(10, Suit.DIAMONDS), new Card(10, Suit.CLUBS),
				new Card(5, Suit.SPADES), new Card(5, Suit.CLUBS) };
		Card[] tend10c5s5h5c = { new Card(10, Suit.HEARTS),
				new Card(10, Suit.DIAMONDS), new Card(5, Suit.SPADES),
				new Card(5, Suit.HEARTS), new Card(5, Suit.CLUBS) };
		Card[] JsJh6h4d4c = Card.createArray("Js, Jh, 6h, 4d, 4c");
		Card[] JsJhJc6h4s4d4c = Card.createArray("Js, Jh, Jc, 6h, 4s, 4d, 4c");
		Card[] JsJhJd3s3c = Card.createArray("Js, Jh, Jd, 3s, 3c");
		Card[] AhKdQsJc10d = Card.createArray("Ah, Kd, Qs, Jc, 10d");
		Card[] KhQhJh10h9h = Card.createArray("Kh, Qh, Jh, 10h, 9h");

		// Card[] cards = Card.createArray("10h, 6s, 6h, Js, Jh");
		// Arrays.sort(cards);
		// // String msg = "Cannot satisfy";
		// IEvaluator fullHouseE = new FullHouseEvaluator(1, 5);
		// System.out.println(fullHouseE.canSatisfy(cards));
		//
		// IEvaluator fh = new FullHouseEvaluator(4, 5);
		// Card[] JsJh3s3d3c = { JsJhJd3s3c[0], JsJhJd3s3c[1], JsJhJd3s3c[3],
		// new Card(3, Suit.DIAMONDS), JsJhJd3s3c[4] };
		// int[] subset = { 0, 1, 2, 3, 4 };
		// Arrays.sort(JsJh3s3d3c);
		// Hand created = fh.createHand(JsJh3s3d3c, subset);
		// System.out.println(created.toString());

		// IEvaluator sf = new StraightFlushEvaluator(5, 5, 13);
		// int[] subset = { 9 };
		// Hand created = sf.createHand(KhQhJh10h9h, subset);
		// String msg =
		// "The cards Kh, Qh, Jh, 10h, and 9h create a straight hand: ";
		// System.out.println(created.toString());

		// IEvaluator fh = new FullHouseEvaluator(4, 5);
		// Hand created = new Hand(JsJhJd3s3c, null, fh);
		// Hand best = fh.getBestHand(JsJhJd3s3c);
		// System.out.println(created.toString());
		// System.out.println(best.toString());

		// IEvaluator tk = new ThreeOfAKindEvaluator(2, 5);
		// Card[] mainCards = { JsJhJc6h4s4d4c[0], JsJhJc6h4s4d4c[1],
		// JsJhJc6h4s4d4c[2] };
		// Card[] sideCards = { JsJhJc6h4s4d4c[3], JsJhJc6h4s4d4c[4],
		// JsJhJc6h4s4d4c[5], JsJhJc6h4s4d4c[6] };
		// Hand created = new Hand(mainCards, sideCards, tk);
		// Hand best = tk.getBestHand(JsJhJc6h4s4d4c);
		// System.out.println(created.toString());
		// System.out.println(best.toString());

		// IEvaluator op = new OnePairEvaluator(2, 5);
		// Card[] all = AsAdKh7c5d;
		// Card[] main = { AsAdKh7c5d[0], AsAdKh7c5d[1] };
		// Card[] side = { AsAdKh7c5d[2], AsAdKh7c5d[3], AsAdKh7c5d[4] };
		// int[] subset = { 0, 1 };
		// Hand same = new Hand(Arrays.copyOf(main, main.length), Arrays.copyOf(
		// side, side.length), op);
		// System.out.println(same.toString());
		// Hand created = op.createHand(all, subset);
		// System.out.println(created.toString());

		// IEvaluator fh = new FullHouseEvaluator(3, 5);
		// Arrays.sort(AhKdQsJc10d);
		// Hand createdbest = fh.getBestHand(AhKdQsJc10d);
		// System.out.println(createdbest.toString());

		// Card[] cards =
		// Card.createArray("6s, Jh, Ah, 10h, 6h, Js, 6c, Kh, Qh");
		// Arrays.sort(cards);
		// int[] subset = { 6, 8 };
		// IEvaluator onePairEval = new OnePairEvaluator(3, 4);
		// Hand hand = onePairEval.createHand(cards, subset);
		// String msg =
		// ("The newly constructed cards[2c, 2d, 3h]'s subset should satisfy the one pair evaluator");
		// assertEquals(msg, "One Pair (3) [6s 6h : Ah Kh]", hand.toString());
		// System.out.println(hand.toString());

		// IEvaluator StraightEvaluator = new StraightEvaluator(1, 5, 9);
		// Card[] cards =
		// Card.createArray("Ah, 5h, 4s, 3s, 2s, 3s, 9s, 8s, 7h, 6c");
		// Arrays.sort(cards);
		// Hand hand = StraightEvaluator.getBestHand(cards);
		// System.out.println(hand.toString());

		// IEvaluator StraightEvaluator = new FullHouseEvaluator(1, 5);
		// Card[] cards =
		// Card.createArray("Kh, Ks, 5d, 5s, 5s, 4c, 4s, 4d, 2c, 2s");
		// Arrays.sort(cards);
		// Hand hand = StraightEvaluator.getBestHand(cards);
		// System.out.println(hand.toString());

		// IEvaluator fh = new FullHouseEvaluator(4, 5);
		// Card[] JsJh3s3d3c = { JsJhJd3s3c[0], JsJhJd3s3c[1], JsJhJd3s3c[3],
		// new Card(3, Suit.DIAMONDS), JsJhJd3s3c[4] };
		// int[] subset = { 0, 1, 2, 3, 4 };
		// Hand created = fh.createHand(JsJh3s3d3c, subset);
		// System.out.println(created.toString());

		// IEvaluator StraightEvaluator = new FullHouseEvaluator(1, 5);
		// Card[] cards =
		// Card.createArray("Kh, Ks, 5d, 5s, 5s, 4c, 4s, 4d, 2c, 2s");
		// Arrays.sort(cards);
		// Hand hand = StraightEvaluator.getBestHand(cards);
		// System.out.println(hand.toString());

		// IEvaluator fk = new FourOfAKindEvaluator(3, 5);
		// Card[] all = Ah9s9h9d9c;
		// Card[] main = { Ah9s9h9d9c[1], Ah9s9h9d9c[2], Ah9s9h9d9c[3],
		// Ah9s9h9d9c[4] };
		// Card[] side = { Ah9s9h9d9c[0] };
		// int[] subset = { 1, 2, 3, 4 };
		// System.out.println(fk.createHand(all, subset).toString());

		// IEvaluator fh = new FullHouseEvaluator(3, 5);
		// Arrays.sort(AhKdQsJc10d);
		// Hand createdbest = fh.getBestHand(AhKdQsJc10d);
		// System.out.println(createdbest.toString());

		Card[] cards = Card.createArray("Ah, 6h, 5s, 4s, 3c");
		Arrays.sort(cards);
		IEvaluator StraightEvaluator = new StraightEvaluator(1, 5, 7);
		Hand createdbest = StraightEvaluator.getBestHand(cards);
		System.out.println(createdbest.toString());
	}

}
