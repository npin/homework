package hw3;

import java.util.ArrayList;

import api.Card;
import api.Hand;

/**
 * Evaluator for a hand containing (at least) four cards of the same rank. The
 * number of cards required is four.
 * 
 * The name of this evaluator is "Four of a Kind".
 */
// Note: You must edit this declaration to extend AbstractEvaluator
// or to extend some other class that extends AbstractEvaluator
public class FourOfAKindEvaluator extends AbstractEvaluator {
	/**
	 * Constructs the evaluator.
	 * 
	 * @param ranking
	 *            ranking of this hand
	 * @param handSize
	 *            number of cards in a hand
	 */
	public FourOfAKindEvaluator(int ranking, int handSize) {
		// TODO: call appropriate superclass constructor and
		// perform other initialization
		super();
		super.ranking = ranking;
		super.handSize = handSize;
		super.numMainCards = 4;
		super.name = "Four of a Kind";
	}

	@Override
	public String getName() {
		return super.getName();
	}

	@Override
	public int getRanking() {
		return super.getRanking();
	}

	@Override
	public int cardsRequired() {
		return super.cardsRequired();
	}

	@Override
	public int handSize() {
		return super.handSize();
	}

	@Override
	public boolean canSatisfy(Card[] mainCards) {
		return super.mainCardsSameKind(mainCards);
	}

	@Override
	public boolean canSubsetSatisfy(Card[] allCards) {
		return super.canSubsetSatisfy(allCards);
	}

	@Override
	public Hand createHand(Card[] allCards, int[] subset) {
		return super.createHand(allCards, subset);
	}

	@Override
	public Hand getBestHand(Card[] allCards) {
		return super.getBestHand(allCards);
	}
}
