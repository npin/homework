package exam3;

import java.util.Arrays;

/**
 * All of them are forked from S.Kautz's code See:
 * http://www.cs.iastate.edu/~smkautz
 * /cs227s14/examples/week10/SelectionSorter.java
 * 
 */
public class SelectionSort {

	public static void main(String[] args) {
		int[] arr = { 5, 3, 4, 7, 6, 2, 4, 3, 1 };
		System.out.println(Arrays.toString(arr));
		selectionSortRange(arr, 2, arr.length - 3);
		System.out.println(Arrays.toString(arr));
	}

	public static void selectionSortRange(int[] arr, int start, int end) {
		// For each position i
		for (int i = start; i < end; i++) {
			// find index of smallest element to the right of i
			int minIndex = findIndexOfMin(arr, i, end);
			swap(arr, i, minIndex);
		}
	}

	private static void swap(int[] arr, int i, int j) {
		int temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}

	private static int findIndexOfMin(int[] arr, int startIndex, int endIndex) {
		int indexOfMin = startIndex;
		for (int j = startIndex + 1; j < endIndex + 1; ++j) {
			if (arr[j] < arr[indexOfMin]) {
				indexOfMin = j;
			}
		}
		return indexOfMin;
	}
}
