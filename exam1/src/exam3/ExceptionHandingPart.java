package exam3;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ExceptionHandingPart {
	public static void main(String[] args) {
		try {
			System.out.println(tryStuff("10 20 23skidoo 30 foo bar"));
			printPercentages("votes.txt");
		} catch (FileNotFoundException e) {
			System.out.println("no such file");
		}
	}

	public static void printPercentages(String filename)
			throws FileNotFoundException {
		File f = new File(filename);
		Scanner s = new Scanner(f);
		// Do something
	}

	public static int tryStuff(String text) {
		int total = 0;
		int i = 0;
		Scanner scanner = new Scanner(text);
		while (scanner.hasNext()) {
			try {
				String s = scanner.next();
				i = Integer.parseInt(s);
				total += i;
			} catch (NumberFormatException nfe) {
				total -= i;
			}
		}
		return total;
	}

}
