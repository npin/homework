package exam3;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class RecursionPart {

	public static void main(String[] args) throws IOException {
		 System.out.println(-1 % 2 == 0);
		 System.out.println(-2 % 2 == 0);
		 mystery(-1);
		 mysteryIteration(0);
		 mystery(0);
		 mysteryIteration(4);
		 mystery(4);
		 mysteryIteration(5);
		 mystery(5);
		 System.out.println(pow(2, 2));
		 System.out.println(pow(2, 3));
		 System.out.println(pow(2, 0));
		 System.out.println(pow(2, 5));
		File f = new File("../project10/src/lab10");
		System.out.println(numberOfFiles(f));
	}

	public static void mystery(int x) {
		if (x == 0) {
			System.out.println("pooh");
		} else if (x % 2 == 0) {
			System.out.println(x);
			mystery(x / 2);
		} else {
			mystery(x - 1);
		}
	}

	public static void mysteryIteration(int x) {
		if (x % 2 == 0) {
			for (int i = x; i >= 2; i -= 2) {
				System.out.println(i);
			}
			System.out.println("pooh");
		} else {
			for (int i = x - 1; i >= 2; i -= 2) {
				System.out.println(i);
			}
			System.out.println("pooh");
		}
	}

	public static int pow(int x, int p) {
		if (p == 0) {
			return 1;
		} else if (p % 2 == 0) {
			return pow(x, p / 2) * pow(x, p / 2);
		} else {
			return x * pow(x, (p - 1) / 2) * pow(x, (p - 1) / 2);
		}
	}

	public static int numberOfFiles(File file) throws IOException {
		int number = 0;
		if (file.isFile()) {
			number = number + 1;
		} else {
			// If it is not file
			// Find
			File[] files = file.listFiles();
			for (int i = 0; i < files.length; i++) {
				number = number + numberOfFiles(files[i]);
			}
		}
		return number;
	}

	public static void mysteryWithoutRecursion(int x) {
		while (x != 0) {
			if (x % 2 == 0) {
				System.out.println(x);
				x /= 2;
			} else {
				x -= 1;
			}
		}
		System.out.println("pooh");
	}

}
