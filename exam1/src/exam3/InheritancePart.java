package exam3;

public class InheritancePart {
	public static void main(String[] args) {
		Item i = new Book("Treasure	Island"); // OK
		System.out.println(i.getTitle()); // Output: �Treasure Island�
		// System.out.println(i.getCheckOutPeriod()); // Compile error, Item has
		// no such method
		Book b = new ReferenceBook("How	to	Bonsai	Your	Pet");
		System.out.println(b.getTitle());
		System.out.println(b.getCheckOutPeriod());
		LibraryItem li = null;
		// li = new LibraryItem("Catch-22"); //Compile error, no Constructor for LibraryItem
		li = new Book("Catch-22");
		System.out.println(li.getTitle());
		System.out.println(li.getCheckOutPeriod());
		li = new DVD("Shanghai	Surprise", 120);
		System.out.println(li.getTitle());
		System.out.println(li.getCheckOutPeriod());
		// System.out.println(li.getDuration());//Compile error, No getDuration method in
		// LibraryItem
		System.out.println(((DVD) li).getDuration());
		i = (Item) b;
//		b = i;//Compile error, Declared type of i is Item and No Downcast
		System.out.println(b.getClass() == i.getClass());
		System.out.println(b instanceof Book);
		System.out.println(i instanceof Book);
		System.out.println(i.getTitle());
//		ReferenceBook rb = (ReferenceBook) b;
//		System.out.println(rb.getClass() == b.getClass());
//		System.out.println(rb.getClass() == i.getClass());
//		System.out.println(rb instanceof Item);
//		System.out.println(rb instanceof Book);
//		System.out.println(rb.getTitle());
	//	rb = (ReferenceBook) new Book("Big	Java"); // Exception
													// ClassCastException Book
													// cannot be cast to
													// ReferenceBook
		// System.out.println(rb.getTitle()); //"Big Java"
	}
}
