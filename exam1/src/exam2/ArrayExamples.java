package exam2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class ArrayExamples {

	public static void main(String[] args) {
		//Problem a
		double[] doubles ={1.2, 3.2, 3.4123, 5.22};
		System.out.println(avg(doubles));
		
		//Problem b
		String sentence = "You are a genius";
		System.out.println(longestWord(sentence));
		
		//Problem c
		System.out.println(nonAlphaReplacing("Hello, world!"));
		
		//Problem d
		System.out.println(doubleBalanceTime(0.12, 1000.0));
		
		//Problem e
		ArrayList<Integer> ints1 = new ArrayList<Integer>();
		ints1.add(3);
		ints1.add(1);
		System.out.println(isIncreasing(ints1));
		
		//Problem f
		System.out.println(vowelIndex("Hello"));
		
		//Problem g
		System.out.println(isLetterRepeating("Hello"));
		System.out.println(isLetterRepeating("He"));
		
		//Problem h
		int[] testInts = {1, 2, 3, 4, 5};
//		reverseInts(testInts);
		
		//Problem i
		System.out.println(isPermutation(testInts));
		int[] testInts2 = {1, 1, 3, 4};
		System.out.println(isPermutation(testInts2));
		
		//Problem j
		reverseDiagonalStars(5);
		
		//Problem k
		double[][] testDoubleTwoD = new double[][]{{1.2, 1.3, 1.3}, {2.2, 2.3, 3.3}};
		System.out.println(Arrays.toString(columnAvg(testDoubleTwoD)));
		
		//Problem l
		int[][] testIntTwoD = new int[][]{{1, 2, 3}, {2, 3, 3}};
		System.out.println(columnWithMaxSum(testIntTwoD));
		
		//Problem m
		System.out.println(twoDArrCreator(2, 2, testInts2));
	}

	public static double avg(double[] values){
		double sum = 0;
		for (int i = 0; i < values.length; i++){
			sum += values[i];
		}
		double avg = sum/values.length;
		return avg;
	}
	
	public static String longestWord(String givenSentence){
		String sentence = givenSentence;
		Scanner scanner = new Scanner(sentence);
		ArrayList<String> words = new ArrayList<String>();
		int longestLength = 0;
		int i = 0;
		int longestPosition = 0;
		scanner.useDelimiter(" ");
		while( scanner.hasNext()){
			words.add(scanner.next());	
			
			//Delete the period or comma
			String word = words.get(i);
			if(word.charAt(word.length()-1) == ',' || word.charAt(word.length()-1) == '.'){
				word = word.substring(0, word.length()-1);
				words.set(i, word);
			}
			
			if(words.get(i).length() > longestLength)
			{
				longestLength = words.get(i).length();
				longestPosition = i;
			}
			
			i ++;
		}
		return words.get(longestPosition);	
	}
	
	public static String nonAlphaReplacing(String s){
		char[] chars = new char[s.length()];
		String result = "";
		for (int i =0; i < s.length(); i ++){
			chars[i] = s.charAt(i);
			if(!Character.isAlphabetic(chars[i])){
				chars[i] = '#';
			}
			result += chars[i];
		}
		return result;
	}
	
	public static int doubleBalanceTime(double annualInterestRate, double initialBalance){
		double balance = initialBalance;
		double doubleBalance = initialBalance * 2;
		double monthlyInterestRate = annualInterestRate/12;
		double monthlyInterest = initialBalance * monthlyInterestRate;
		int months = 0;
		while(balance <= doubleBalance){
			balance += monthlyInterest;
			months ++;
			if (months % 12 == 0){
				monthlyInterest = balance * monthlyInterestRate;
			}
		}
		
		return months;
	}
	
	public static boolean isIncreasing(ArrayList<Integer> ints){
		int i = 0;
		boolean isIncreasing = true;
		while(i < ints.size()-1){
			if(ints.get(i) > ints.get(i + 1)){
				isIncreasing = false;
				return isIncreasing;
			}
			i ++;
		}
		return isIncreasing;
	}
	
	public static int vowelIndex(String s){
		boolean foundVowel = false;
		int firstVowel = -1;
		int i = 0;
		ArrayList<Character> chars = stringToChars(s);
		while(i < s.length() && !foundVowel){
			if(isVowel(chars.get(i))){
				firstVowel = i;
				foundVowel = true;
			}
			i++;
		}
		return firstVowel;
	}
	
	private static boolean isVowel(char c){
		if(c == 'a' || c == 'A' || c == 'e' || c == 'E' || c == 'i' || c == 'I' || c == 'o' || c == 'O' || c == 'u' || c == 'U'){
			return true;
		}
		else{
			return false;
		}
	}
	
	private static ArrayList<Character> stringToChars(String s){
		ArrayList<Character> chars = new ArrayList<Character>();
		for(int i = 0; i < s.length(); i++){
			chars.add(s.charAt(i));
		}
		return chars;
	}
	
	public static boolean isLetterRepeating(String s){
		ArrayList<Character> chars = stringToChars(s);
		boolean repeating = false;
		int i = 0;
		while(!repeating && i < chars.size()){
			ArrayList<Character> tempChars = new ArrayList<Character>(chars);
			tempChars.remove(i);
			if(findLetter(chars.get(i), tempChars)){
				repeating = true;
			}
			i++;
		}
		return repeating;
	}
	
	public static void reverseInts(int[] ints){
		for(int i = 0; i < ints.length/2; i ++ ){
			int swap = ints[i];
			ints[i] = ints[ints.length-i-1];
			ints[ints.length-i-1] = swap;
		}
	}
	
	public static boolean isPermutation(int[] ints){
		boolean repeating = false;
		int i = 0;
		ArrayList<Integer> intsList = arrToList(ints);
		while(!repeating && i < ints.length){
			ArrayList<Integer> tempInts = new ArrayList<Integer>(intsList);
			tempInts.remove(i);
			if(findInt(intsList.get(i), tempInts)){
				repeating = true;
			}
			i++;
		}
		return !repeating;
	}
	
	public static ArrayList<Integer> arrToList(int[] ints){
		ArrayList<Integer> intsList = new ArrayList<Integer>();
		for (int i = 0 ; i < ints.length; i ++){
			intsList.add(ints[i]);
		}
		return intsList;
	}
	
	public static boolean findInt(int number, ArrayList<Integer> ints){
		boolean found = false;
		int i = 0;
		while(!found && i < ints.size()){
			if(ints.get(i) == number){
				found = true;
			}
			i ++;
		}
		return found;
	}
	private static boolean findLetter(char c, ArrayList<Character> chars){
		boolean found = false;
		int i = 0;
		while(!found && i < chars.size()){
			if(chars.get(i) == c){
				found = true;
			}
			i ++;
		}
		return found;
	}
	
	public static void reverseDiagonalStars(int n){
		for(int rows = 0; rows < n; rows ++){
			for(int columns = 0; columns < n-rows-1; columns++){
				System.out.print(" ");
			}
			System.out.print("*");
			System.out.println();
		}
	}
	
	public static double[] columnAvg(double[][] twoDArray){
		double[] avgColumn = new double[twoDArray[0].length];
		double[] sumColumn = new double[twoDArray[0].length]; 
		for(int rows = 0; rows < twoDArray.length; rows ++){
			for(int columns = 0; columns < twoDArray[rows].length; columns++ ){
				sumColumn[columns] += twoDArray[rows][columns];
			}			
		}
		for(int columns = 0; columns < twoDArray[0].length; columns++){
			avgColumn[columns] = sumColumn[columns]/twoDArray.length;
		}
		return avgColumn;
	}
	
	public static int columnWithMaxSum(int[][] ints){
		int[] sumColumn = new int[ints[0].length]; 
		int maxColumn = 0;
		for(int rows = 0; rows < ints.length; rows ++){
			for(int columns = 0; columns < ints[rows].length; columns++ ){
				sumColumn[columns] += ints[rows][columns];
			}			
		}
		for(int columns = 0; columns < ints[0].length; columns++){
			if(sumColumn[columns] > sumColumn[maxColumn]){
				maxColumn = columns;
			}
		}
		return maxColumn;
	}
	
	public static int[][] twoDArrCreator(int w, int h, int[] arr){
		if(! (w*h == arr.length)){
			return null ;
		}
		int[][] arrTwoD = new int[w][h];
		for(int rows = 0 ; rows < w; rows ++){
			for(int columns = 0; columns < h; columns++){
				arrTwoD[rows][columns] = arr[w*rows + columns];
			}
		}
		
		return arrTwoD;
		
	}
}
