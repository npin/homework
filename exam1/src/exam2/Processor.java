package exam2;

public interface Processor {
	public double getResult();

	public void process(double valToProcess);
}
