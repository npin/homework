package exam1;

import java.awt.Rectangle;
import java.util.Random;
import java.util.Scanner;

public class Exam1Tester {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		//Problem 1
		Exam1 ex = new Exam1(3);
		System.out.println(ex.foo(2, 1));
		System.out.println(ex.foo(0, -1));
		System.out.println(ex.foo(1, 1));
		
		//Problem 2
		double x = 2.4; 
		int j = 13; 
		String w = "food"; 
		
		System.out.println(x > 0);
		System.out.println(j / 3);
		System.out.println(j % 3);
		// System.out.println(j + 1 = j ); //java.lang.Error
		System.out.println(j / 10.0);
		System.out.println(w + w);
		System.out.println(j + 1 != j);
		System.out.println(j + "");
		//System.out.println(j.length()); //java.lang.Error
		System.out.println(w.length());
		
		//Problem 3
		int a = 42; 
		Rectangle rect = new Rectangle(10, 20, 30, 40); // (x, y, width, height)
		Rectangle rect2 = new Rectangle(2, 4, 6, 8); 
		int b = a; 
		Rectangle rect3 = rect; 
		rect3.setSize(99, 40); 
		rect2.setLocation(137, 20); 
		b = b + 5; 
		System.out.println(a); 
		System.out.println(b); 
		System.out.println(rect.getWidth()); 

		//Problem 4
		String newID = createID("Steve", "Kautz");
		System.out.println(newID);
		
		//Problem 5
		//BusTester Class
		
		//Problem 6
		//Loan Class
		Loan ln = new Loan(1000.00, 0.24);
		System.out.println("Print Balance, expected 1000.00");
		System.out.println(ln.getBalance())	;
		ln.makePayment(100.0);
		System.out.println("Print Balance, expected 920.0");
		System.out.println(ln.getBalance())	;
		System.out.println("Print Pay of Amount, expected 938.40");
		System.out.println(ln.getPayofAmount())	;
		
		//Problem 7
		//Door
		
		//Problem 8
		
		final double SINGLEDONUT = 0.75;
		final double DOZENDONUT = 8.00;
		final double SINGLECOFFE = 1.50;
		System.out.println("Please Enter the Number of donuts");
		Scanner in = new Scanner(System.in);
		int donuts = in.nextInt();
		System.out.println("Please Enter the Number of coffee");
		int coffee = in.nextInt();
		int dozenOfDonuts = donuts / 12;
		int sigleDonuts = donuts - dozenOfDonuts * 12;
		double price = Math.max(coffee - dozenOfDonuts * 2, 0)*SINGLECOFFE + sigleDonuts * SINGLEDONUT + dozenOfDonuts * DOZENDONUT;
		System.out.println("The whole price is " +price);
		
		//Problem 9
		//ElectricityBill
		ElectricBill ebWinter = new ElectricBill(1050, false);
		System.out.println("The Total Cost For winter is " + ebWinter.computeTotal());
		ElectricBill ebSummer = new ElectricBill(1050, true);
		System.out.println("The Total Cost For summer is " + ebSummer.computeTotal());
		
		//Problem 10
		int x1 = 42; 
		String y1 = "lunchtime"; 
		System.out.println(y1.equals("breakfast") || x1 != 42);
		System.out.println(x1 == 5 || (y1.length() > 0 && !(x1 <= 0)));
		System.out.println(!((x1 > 0 && x1 > 1) || x1 > 2) );
		System.out.println("x is at least 25");
		System.out.println(x1 >= 25);
		System.out.println("x is between 25 and 50 inclusive");
		System.out.println(x1 >= 25 && x1 <= 50);
		System.out.println("it is not the case that x is between 25 and 50 inclusive");
		System.out.println(!(x1 >= 25 && x1 <= 50));
		System.out.println("y is either 10 or 12 characters long");
		System.out.println(y1.length() == 10 || y1.length() == 12);
		System.out.println("x is equal to the three times the length of y");
		System.out.println(x1 == 3 * y1.length());
		System.out.println("the length of y is not divisible by 3");
		System.out.println(!(y1.length() % 3 == 0));
		System.out.println("x is negative or is even and divisible by 3");
		System.out.println((x1 < 0 || x1 % 2 == 0) && (x1 % 3 == 0));
		System.out.println("y is not equal to the string �dinner�");
		System.out.println(!(y1.equals("dinner")));
	}
	public static String createID(String firstName, String lastName){
		String initial = firstName.substring(0, 1).toLowerCase();
		String last = lastName.toLowerCase();
		Random rand = new Random();
		int numberExtension = rand.nextInt(10)+1;
		return initial + last + numberExtension;
	}
	
	

}
