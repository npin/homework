package exam1;

public class BusTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Bus cyride = new Bus();
		System.out.println("At the first stop, 5 passengers get on");
		System.out.println("Expected: currentPassenger = 5");
		cyride.busStop(5, 0);
		System.out.println(cyride.passengerCount());
		System.out.println("Expected: maximumPassenger = 5");
		System.out.println(cyride.maximumPassengerCount());
		System.out.println("Expected: busStop = 1");
		System.out.println(cyride.numberOfStops());
		cyride.busStop(7, 2);
		System.out.println("At the second stop, 2 get off and 7 more get on");
		System.out.println("Expected: currentPassenger = 10");
		System.out.println(cyride.passengerCount());
		System.out.println("Expected: maximumPassenger = 10");
		System.out.println(cyride.maximumPassengerCount());
		System.out.println("Expected: busStop = 2");
		System.out.println(cyride.numberOfStops());
		cyride.busStop(6, 1);
		System.out.println("At the third stop, 1 get off and 6 more get on");
		System.out.println("Expected: currentPassenger = 15");
		System.out.println(cyride.passengerCount());
		System.out.println("Expected: maximumPassenger = 15");
		System.out.println(cyride.maximumPassengerCount());
		System.out.println("Expected: busStop = 3");
		System.out.println(cyride.numberOfStops());
		cyride.busStop(0, 5);
		System.out.println("At the fourth stop, 5 get off");
		System.out.println("Expected: currentPassenger = 10");
		System.out.println(cyride.passengerCount());
		System.out.println("Expected: maximumPassenger = 15");
		System.out.println(cyride.maximumPassengerCount());
		System.out.println("Expected: busStop = 4");
		System.out.println(cyride.numberOfStops());
	}

}
