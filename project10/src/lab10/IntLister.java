package lab10;

public class IntLister {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		IntListSorted list = new IntListSorted();

		list.add(5);
		list.add(4);
		list.add(3);
		System.out.println(list);
		System.out.println("Size: " + list.size());
		System.out.println("Min: " + list.getMinimum());
		System.out.println("Max: " + list.getMaximum());
		System.out.println("Median " + list.getMedian());
		
		IntListSorted list2 = new IntListSorted();

		list2.add(1);
		list2.add(3);
		list2.add(10);
		list2.add(0);
		System.out.println(list2);
		System.out.println("Size: " + list2.size());
		System.out.println("Min: " + list2.getMinimum());
		System.out.println("Max: " + list2.getMaximum());
		System.out.println("Median " + list2.getMedian());
	}

}
