package lab7;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class LineNumberer {
	public static void main(String[] args) throws FileNotFoundException{
//		Scanner scanner = new Scanner(System.in);
		int lineCount = 1;
		
//		while (scanner.hasNextLine()){
//			String line = scanner.nextLine();
//			System.out.print(lineCount + "");
//			System.out.println(line);
//			lineCount += 1;
//		}
		
		//Input is LineNumberer.java

		// open the file
		File file = new File("./src/lab7/LineNumberer.java");
		Scanner scanner = new Scanner(file);
		int lineCount2 = 1;
		
		while (scanner.hasNextLine()){
			String line = scanner.nextLine();
			System.out.print(lineCount2 + "");
			System.out.println(line);
			lineCount2 += 1;
		}
		scanner.close();
		
		// Output is myDocument2.txt
		Scanner in = new Scanner(System.in);
		File outFile = new File("myDocument2.txt");
		PrintWriter out = new PrintWriter(outFile);
		
		//Echo keyboard input out to the file
		while (in.hasNextLine()){
			String line = in.nextLine();
			out.println(lineCount + "");
			out.println(line);
			lineCount += 1;
		}
		
		out.close();
	}
}
