package lab3;

public class RabbitModel1 {
	//The population starts at zero and increases each year by 1 rabbit,
	// but after every 5 years, oversaturation brings the population back down to 0. 
	//(Hint: consider an expression using the mod operator "%". If you divide the population by 5, what is the remainder?)
	private int population;

	//Constructors
	public RabbitModel1(){
		reset();
	}
	// Methods getPopulation  that returns an int representing the number of rabbits alive at the end of the current year of the simulation
	public int getPopulation () 
	{
		return population;
	}
	
	// method named simulateYear that simulates the passage of a year. 
	public void simulateYear()
	{
		if (population % 5 == 0) // Over 5 years, population goes back to 0
		{
			reset();
		}
		else
		{
		population ++;
		}
	}
	
	//  method named reset which initializes any instance variables to their values at the start of the simulation. It takes no arguments.
	public void reset()
	{
		population = 0 ;
	}
}
