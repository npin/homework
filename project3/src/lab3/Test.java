package lab3;


import java.util.Random;

public class Test {

	public static void main(String[] args) {
		// What exception do you get if you divide a number by zero?
		 //System.out.println(5/0);
		// Write a main method that prints out these two values Integer.MAX_VALUE and Integer.MIN_VALUE
		System.out.println(Integer.MAX_VALUE);
		System.out.println(Integer.MIN_VALUE);
		// suppose you have a variable whose value is Integer.MAX_VALUE. What is the result of adding 1 to it?
		System.out.println(Integer.MAX_VALUE+1);
		//Do you get an error? How about adding 2? What do you suppose you'd get by adding Integer.MAX_VALUE to Integer.MAX_VALUE?
		System.out.println(Integer.MAX_VALUE+2);
		//To use it, you first construct an instance of the class Random:
		//Random rand = new Random();
		Random rand = new Random(137);
		//Try it. Create a class in your lab3 package with a main method. 
		System.out.println(rand.nextInt(6));
		System.out.println(rand.nextInt(6));
		System.out.println(rand.nextInt(6));
		System.out.println(rand.nextInt(6));
		
	}
		

}
