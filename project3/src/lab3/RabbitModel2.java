package lab3;

public class RabbitModel2 {
	//The population starts at 500 and decreases by half each year.
	private int population;

	//Constructors
	public RabbitModel2(){
		reset();
	}
	// Methods getPopulation  that returns an int representing the number of rabbits alive at the end of the current year of the simulation
	public int getPopulation () 
	{
		return population;
	}
	
	// method named simulateYear that simulates the passage of a year. 
	public void simulateYear()
	{
		
		if (population % 2 == 0) // When the number of the population is even
		{
			population = population/2;
		}
		else // When the number of the population is odd
		{
			population = (population - 1)/2;
		}
	}
	
	//  method named reset which initializes any instance variables to their values at the start of the simulation. It takes no arguments.
	public void reset()
	{
		population = 500 ;
	}
}
