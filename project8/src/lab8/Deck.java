package lab8;

import java.util.ArrayList;
import java.util.Random;

import lab8.Card.Suit;

/**
 * Class representing a standard 52-card deck of playing cards from which cards
 * can be selected at random.
 */
public class Deck {
	/**
	 * The cards comprising this deck.
	 */
	private Card[] cards;

	/**
	 * The random number generator to use for selecting cards.
	 */
	private Random rand;
	
	private ArrayList<Card> cardsList = new ArrayList<Card>();
	
	/**
	 * Constructs a new deck with a default random number generator.
	 */
	public Deck() {
		rand = new Random();
		init();
		cardsList = cardListGenerator(cards);
	}

	/**
	 * Constructs a new deck with the given random number generator.
	 */
	public Deck(Random givenGenerator) {
		rand = givenGenerator;
		init();
		cardsList = cardListGenerator(cards);
	}

	/**
	 * Returns a new array containing k elements selected at random from this
	 * deck.
	 */
	public Card[] select(int k) {
		// Repeat selectAndDelete for k times
		Card[] randomCards = selectFromCardsList(k, cardsList);
		
		return randomCards;
	}

	private Card[] selectFromCardsList(int k, ArrayList<Card> cardsLs) {
		Card[] randomCards = new Card[k];
		
		for (int i = 0; i < k; ++i)
		{
			randomCards[i] = selectAndDelete(cardsLs);
		}		
		
		return randomCards;
	}
	

	private ArrayList<Card> cardListGenerator(Card[] cardsArray) {
		ArrayList<Card> cardsArrayList = new ArrayList<Card>();

		for (int i = 0; i < cardsArray.length; ++i)
		{
			cardsArrayList.add(cardsArray[i]);
		}
		return cardsArrayList;
	}

	private Card selectAndDelete(ArrayList<Card> cardsLs) {
		int randomPosition = rand.nextInt(cardsLs.size());
		Card randomCard = cardsLs.get(randomPosition);
		Card lastCard = cardsLs.get(cardsLs.size() - 1);
		cardsLs.set(randomPosition, lastCard);
		// Delete the last card and decrease 1 of the array list
		cardsLs.remove(cardsLs.size() - 1);
		return randomCard;
	}

	/**
	 * Initializes a new deck of 52 cards.
	 */
	private void init() {
		cards = new Card[52];
		int index = 0;
		for (int rank = 1; rank <= 13; ++rank)
		{
			cards[index] = new Card(rank, Suit.CLUBS);
			index += 1;
			cards[index] = new Card(rank, Suit.DIAMONDS);
			index += 1;
			cards[index] = new Card(rank, Suit.HEARTS);
			index += 1;
			cards[index] = new Card(rank, Suit.SPADES);
			index += 1;
		}

	}
}
