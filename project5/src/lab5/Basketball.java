package lab5;

public class Basketball {
	private double circumference;
	private boolean isInflated;
	public Basketball(double givenCircumference)
	  {
		circumference = givenCircumference;
		isInflated= false;
	  }

	  public boolean isDribbleable()
	  {
		  if (isInflated)
		  {
			  return true;
		  }
		  else
		  {
			  return false;
		  }
	  }

	  public double getCircumference()
	  {
	    return circumference;
	  }

	  public void inflate()
	  {
		  if (!isInflated)
		  {
			  isInflated = true;
		  }
	  }
}
