package lab6;

public class StringUtilTest {
	public static void main(String[] args) {
		// should return "ready"
	    System.out.println(StringUtil.fixSpelling("raedy"));
	 // should return "achieve"
	    System.out.println(StringUtil.fixSpelling("acheive"));
	    // should return "blue"
	    System.out.println(StringUtil.fixSpelling("bleu"));
	    // should return "reach"
//	    System.out.println(StringUtil.fixSpelling("raech"));
	    
	}
}
