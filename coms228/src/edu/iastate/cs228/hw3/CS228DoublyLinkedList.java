package edu.iastate.cs228.hw3;

import java.util.Iterator;
import java.util.ListIterator;
import java.util.NoSuchElementException;

/**
 * A doubly linked list class for Com S 228
 * 
 * @author Siyu Lin
 * 
 * @param <T>
 */
public class CS228DoublyLinkedList<T> implements CS228List<T> {

	/**
	 * The size of the doubly linked list
	 */
	private int size;
	/**
	 * A dummy node indicating the head of the doubly linked list
	 */
	private Node head;
	/**
	 * A dummy node indicating the tail of the doubly linked list
	 */
	private Node tail;

	/**
	 * Construct a new doubly linked list with only head and tail
	 */
	public CS228DoublyLinkedList() {
		head = new Node();
		tail = new Node();
		clear();
	}

	/**
	 * Appends the specified element to the end of this list
	 * 
	 * @param toAdd
	 *            Element to add to the end of the list
	 */
	@Override
	public void add(T toAdd) {
		if (toAdd == null) {
			throw new NullPointerException();
		}
		Node temp = new Node(toAdd);
		link(tail.previous, temp);
		++size;
	}

	/**
	 * Inserts the specified element at the specified position in this list,
	 * shifts elements to the right if necessary
	 * 
	 * @param index
	 *            Add the element at this logical index of the list
	 * @param toAdd
	 *            Element to add to the list
	 * @return returns true if add is successful, false if not
	 * @throws IllegalArgumentException
	 *             throws IllegalArgumentException if index>size
	 */
	@Override
	public void add(int index, T toAdd) throws IllegalArgumentException {
		if (index < 0 || index > size)
			throw new IllegalArgumentException("" + index);

		if (toAdd == null) {
			throw new NullPointerException();
		}
		Node temp = new Node(toAdd);
		Node predecessor = findNodeByIndex(index - 1);
		link(predecessor, temp);
		++size;
	}

	/**
	 * Compares the specified object with this list for equality.
	 * 
	 * @param o
	 *            Object to compare to
	 * @return
	 */
	public boolean equals(Object o) {
		if (o.getClass() != this.getClass()) {
			return false;
		} else if (o == null) {
			throw new NullPointerException();
		} else {
			CS228DoublyLinkedList<T> other = (CS228DoublyLinkedList<T>) o;
			if (other.size() != size()) {
				return false;
			} else {
				Node current = head.next;
				for (int i = 0; i < size; i++) {
					if (!current.data.equals(other.get(i))) {
						return false;
					}
					current = current.next;
				}
				return true;
			}

		}
	}

	/**
	 * Clears list of all elements
	 */
	@Override
	public void clear() {
		size = 0;
		head.previous = null;
		head.next = tail;
		tail.previous = head;
		tail.next = null;
	}

	/**
	 * Tells us if the list is empty
	 * 
	 * @return true if there are no elements in the list
	 */
	@Override
	public boolean isEmpty() {
		return size == 0;
	}

	/**
	 * Returns element at given index
	 * 
	 * @param index
	 *            index of list
	 * @return element at index
	 * @throws IllegalArgumentException
	 *             throw exception if index<0 or index>size
	 */
	@Override
	public T get(int index) throws IllegalArgumentException {
		// Note ">="
		if (index < 0 || index >= size)
			throw new IndexOutOfBoundsException("" + index);
		return findNodeByIndex(index).data;
	}

	/**
	 * Returns the index of the first occurrence of the specified element in
	 * this list, or -1 if this list does not contain the element.
	 * 
	 * @param e
	 *            Object to compare to
	 * @return
	 */
	@Override
	public int indexOf(Object e) {
		// Precondition: The list should not be empty and e should not be null
		if (isEmpty()) {
			throw new IllegalArgumentException();
		} else if (e == null) {
			throw new NullPointerException();
		} else {
			Node current = head.next;
			for (int i = 0; i < size; i++) {
				if (current.data == e || e != null && current.data.equals(e)) {
					return i;
				}
				current = current.next;
			}
		}

		// If we cannot find the index, return -1;
		return -1;
	}

	/**
	 * Create a forward-only iterator
	 * 
	 * @return
	 */
	@Override
	public Iterator<T> iterator() {
		return new CS228ListIterator();
	}

	/**
	 * Create a list-iterator at index 0
	 * 
	 * @return Returns a list iterator over the elements in this list (in proper
	 *         sequence).
	 */
	@Override
	public ListIterator<T> listIterator() {
		return new CS228ListIterator();
	}

	/**
	 * Create a list-iterator at given index
	 * 
	 * @param index
	 *            index to create iterator
	 * @return
	 * @throws IllegalArgumentException
	 */
	@Override
	public ListIterator<T> listIterator(int index)
			throws IllegalArgumentException {
		if (index < 0 || index > size()) {
			throw new IllegalArgumentException();
		} else {
			return new CS228ListIterator(index);
		}
	}

	/**
	 * Remove element at a given index
	 * 
	 * @param index
	 *            index of element to remove
	 * @return return the element that was removed
	 * @throws IllegalArgumentException
	 *             throw exception if index<0 or index>size
	 */
	@Override
	public T remove(int index) throws IllegalArgumentException {
		if (index < 0 || index >= size) {
			throw new IllegalArgumentException();
		} else {
			Node nodeToRemove = findNodeByIndex(index);
			unlink(nodeToRemove);
			--size;
			return nodeToRemove.data;
		}
	}

	/**
	 * Removes the first occurrence of the specified element from this list, if
	 * it is present
	 * 
	 * @param object
	 *            object to remove
	 * @return true if element was successfully removed
	 */
	@Override
	public boolean remove(T object) {
		if (indexOf(object) == -1) {
			return false;
		} else if (object == null) {
			throw new NullPointerException();
		} else {
			int objectPosition = indexOf(object);
			unlink(findNodeByIndex(objectPosition));
			--size;
			return true;
		}
	}

	/**
	 * Get the size (number of elements) in the list
	 * 
	 * @return return the size of the list
	 */
	@Override
	public int size() {
		return size;
	}

	/**
	 * Replaces the element at the specified position in this list with the
	 * specified element
	 * 
	 * @param index
	 *            index of element to replace
	 * @param element
	 *            element to place in list
	 * @return return element that was replaced
	 * @throws IllegalArgumentException
	 *             throw exception if index<0 or index>size
	 */
	@Override
	public T set(int index, T element) throws IllegalArgumentException {
		if (index < 0 || index >= size) {
			throw new IllegalArgumentException();
		} else if (element == null) {
			throw new NullPointerException();
		} else {
			Node nodeToReplace = findNodeByIndex(index);
			Node nodeToAdd = new Node(element);
			Node nodeToLink = nodeToReplace.previous;
			unlink(nodeToReplace);
			link(nodeToLink, nodeToAdd);
			return nodeToLink.next.data;
		}
	}

	// Copy from Book
	/**
	 * Removes current from the list without updating size.
	 */
	private void unlink(Node current) {
		current.previous.next = current.next;
		current.next.previous = current.previous;
	}

	// Copy from Book
	/**
	 * Inserts newNode into the list after current without updating size.
	 * Precondition: current != null, newNode != null
	 */
	private void link(Node current, Node newNode) {
		newNode.previous = current;
		newNode.next = current.next;
		current.next.previous = newNode;
		current.next = newNode;
	}

	/**
	 * Returns the Node whose index is pos, which will be head if pos = -1 and
	 * tail if pos = size Precondition: size >= pos >= -1
	 */
	private Node findNodeByIndex(int pos) {
		if (pos == -1)
			return head;
		if (pos == size)
			return tail;

		Node current = head.next;
		int count = 0;
		while (count < pos) {
			current = current.next;
			++count;
		}
		return current;
	}

	/**
	 * Implementation of ListIterator for this class
	 * 
	 */
	private class CS228ListIterator implements ListIterator<T> {

		// Class invariants:
		// 1) logical cursor position is always between cursor.previous and
		// cursor
		// 2) after a call to next(), cursor.previous refers to the node just
		// returned
		// 3) after a call to previous() cursor refers to the node just returned
		// 4) index is always the logical index of node pointed to by cursor
		// 5) direction is BEHIND if last operation was next(),
		// AHEAD if last operation was previous(), NONE otherwise

		// direction for remove() and set()
		private static final int BEHIND = -1;
		private static final int AHEAD = 1;
		private static final int NONE = 0;

		private Node cursor;
		private int index;
		private int direction;

		public CS228ListIterator() {
			this(0);
		}

		public CS228ListIterator(int pos) {
			if (pos < 0 || pos > size()) {
				throw new IllegalArgumentException("" + pos);
			}
			cursor = findNodeByIndex(pos);
			index = pos;
			direction = NONE;
		}

		/**
		 * Returns true if this list iterator has more elements when traversing
		 * the list in the forward direction.
		 */
		@Override
		public boolean hasNext() {
			return index < size;
		}

		/**
		 * Returns the next element in the list and advances the cursor
		 * position.
		 */
		@Override
		public T next() {
			if (!hasNext())
				throw new NoSuchElementException();
			T ret = cursor.data;
			cursor = cursor.next;
			++index;
			direction = BEHIND;
			return ret;
		}

		/**
		 * Returns true if this list iterator has more elements when traversing
		 * the list in the reverse direction.
		 */
		@Override
		public boolean hasPrevious() {
			return index > 0;
		}

		/**
		 * Returns the previous element in the list and moves the cursor
		 * position backwards.
		 */
		@Override
		public T previous() {
			if (!hasPrevious())
				throw new NoSuchElementException();

			cursor = cursor.previous;
			--index;
			direction = AHEAD;
			return cursor.data;
		}

		/**
		 * Returns the index of the element that would be returned by a
		 * subsequent call to next().
		 */
		@Override
		public int nextIndex() {
			return index;
		}

		/**
		 * Returns the index of the element that would be returned by a
		 * subsequent call to previous().
		 */
		@Override
		public int previousIndex() {
			return index - 1;
		}

		/**
		 * Removes from the list the last element that was returned by next() or
		 * previous()
		 */
		@Override
		public void remove() {
			if (direction == NONE) {
				throw new IllegalStateException();
			}

			else {
				if (direction == AHEAD) {
					// remove node at cursor and move to next node
					Node n = cursor.next;
					unlink(cursor);
					cursor = n;
				} else {
					// remove node behind cursor and adjust index
					unlink(cursor.previous);
					--index;
				}
			}
			--size;
			direction = NONE;

		}

		/**
		 * Replaces the last element returned by next() or previous() with the
		 * specified element
		 */
		@Override
		public void set(T e) {
			if (direction == NONE) {
				throw new IllegalStateException();
			}
			if (e == null) {
				throw new NullPointerException();
			}
			if (direction == AHEAD) {
				cursor.data = e;
			} else {
				cursor.previous.data = e;
			}

		}

		/**
		 * Inserts the specified element into the list
		 */
		@Override
		public void add(T e) {
			if (e == null) {
				throw new NullPointerException();
			}
			Node temp = new Node(e);
			link(cursor.previous, temp);
			++index;
			++size;
			direction = NONE;
		}

	}

	/**
	 * Node type for this linked list.
	 * 
	 */
	private class Node {
		public T data;
		public Node next;
		public Node previous;

		public Node(T data) {
			if (data == null) {
				throw new IllegalArgumentException();
			}
			this.data = data;
		}

		// Only used to construct the dummy node
		private Node() {
			this.data = null;
		}
	}

}
