package edu.iastate.cs228.hw2;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Comparator;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;


public class EngineTest {
	Pair<Integer, Double> pair1;
	Pair<Integer, Double> pair2;
	Pair<Integer, Double> pair3;
	Pair<Integer, Double> pair4;
	Pair<Integer, Double>[] pairArray;
	Comparator<Pair<Integer, Double>> pairComparator;
	Pair<Engine, Engine> pe1;
	Pair<Engine, Engine> pe2;
	Pair<Engine, Engine> pe3;
	Pair<Engine, Engine> pe4;
	Comparator<Pair<Engine, Engine>> peComparator;
	Pair<Engine, Engine>[] peArray;

	@Before
	public void setup() {
		pair1 = new Pair(5, 0.1);
		pair2 = new Pair(4, 4.4);
		pair3 = new Pair(1, 5.8);
		pair4 = new Pair(4, 2.6);
		pairArray = (Pair<Integer, Double>[]) new Pair<?, ?>[4];
		pairArray[0] = pair1;
		pairArray[1] = pair2;
		pairArray[2] = pair3;
		pairArray[3] = pair4;
		pairComparator = new Comparator<Pair<Integer, Double>>() {

			@Override
			public int compare(Pair<Integer, Double> o1, Pair<Integer, Double> o2) {
				return o1.compareTo(o2);
			}
		};
		peComparator = new Comparator<Pair<Engine, Engine>>() {

			@Override
			public int compare(Pair<Engine, Engine> o1, Pair<Engine, Engine> o2) {
				return o1.compareTo(o2);
			}
		};
		
		pe1 = new Pair(new Engine(131.55, "LockheedMartian"), new Engine(148.82, "NorthrupGreenman"));
		pe2 = new Pair(new Engine(135.68, "LockheedMartian"), new Engine(153.28, "LockheedMartian"));
		pe3 = new Pair(new Engine(52.66, "NorthrupGreenman"), new Engine(17.27, "Boing"));
		pe4 = new Pair(new Engine(119.86, "Boing"), new Engine(3.57, "Boing"));
		
		peArray = (Pair<Engine, Engine>[]) new Pair<?, ?>[4];
		peArray[0] = pe4;
		peArray[1] = pe3;
		peArray[2] = pe2;
		peArray[3] = pe1;
	}

	@Ignore
	@Test
	public void testCompare() {
		Engine lh = new Engine(12, "LockheedMartian");
		Engine ng = new Engine(12, "NorthrupGreenman");
		assertTrue(ng.compareTo(lh) > 0);
	}

	@Ignore
	@Test
	public void testPairCompare() {
		assertTrue(pair1.compareTo(pair2) > 0);
	}

	@Ignore
	@Test
	public void testArraysSort() {
		Arrays.sort(pairArray);
		assertTrue(pairArray[0].compareTo(pairArray[1]) < 0);
		assertTrue(pairArray[3].compareTo(pairArray[2]) > 0);
	}

	@Ignore
	@Test
	public void testInsertionSort() {
		System.out.println(Sort.insertionSort(pairArray, pairComparator));
		assertTrue(pairArray[1].compareTo(pairArray[2]) < 0);
		assertTrue(pairArray[3].compareTo(pairArray[2]) > 0);
	}

	@Ignore
	@Test
	public void testSelectionSort() {
		System.out.println(Sort.selectionSort(pairArray, pairComparator));
		assertTrue(pairArray[1].compareTo(pairArray[2]) < 0);
		assertTrue(pairArray[3].compareTo(pairArray[2]) > 0);
	}

	@Ignore
	@Test
	public void testQuickSortFirst() {
		System.out.println(Sort.quickSort(pairArray, pairComparator, "first"));
		assertTrue(pairArray[1].compareTo(pairArray[2]) < 0);
		assertTrue(pairArray[3].compareTo(pairArray[2]) > 0);
		
	}
	
//	@Ignore
	@Test
	public void testQuickSortMedian() {
		System.out.println(Sort.quickSort(pairArray, pairComparator, "median"));
		assertTrue(pairArray[1].compareTo(pairArray[2]) < 0);
		assertTrue(pairArray[3].compareTo(pairArray[2]) > 0);
		
	}
	
//	@Ignore
	@Test
	public void testQuickSortRandom() {
		System.out.println(Sort.quickSort(pairArray, pairComparator, "random"));
		assertTrue(pairArray[1].compareTo(pairArray[2]) < 0);
		assertTrue(pairArray[3].compareTo(pairArray[2]) > 0);
		
	}
	
	@Ignore
	@Test
	public void testInsertionSortPE() {
		System.out.println(Sort.insertionSort(peArray, peComparator));
		assertTrue(peArray[1].compareTo(peArray[2]) < 0);
		assertTrue(peArray[3].compareTo(peArray[2]) > 0);
	}
	
	@Ignore
	@Test
	public void testSelectionSortPE() {
		System.out.println(Sort.selectionSort(peArray, peComparator));
		assertTrue(peArray[1].compareTo(peArray[2]) < 0);
		assertTrue(peArray[3].compareTo(peArray[2]) > 0);
	}
	
	@Ignore
	@Test
	public void testQuickSortFirstPE() {
		System.out.println(Sort.quickSort(peArray, peComparator, "first"));
		assertTrue(peArray[1].compareTo(peArray[2]) < 0);
		assertTrue(peArray[3].compareTo(peArray[2]) > 0);
		
	}
	
//	@Ignore
	@Test
	public void testQuickSortMedianPE() {
		System.out.println(Sort.quickSort(peArray, peComparator, "median"));
		assertTrue(peArray[1].compareTo(peArray[2]) < 0);
		assertTrue(peArray[3].compareTo(peArray[2]) > 0);
		
	}
	
//	@Ignore
	@Test
	public void testQuickSortRandomPE() {
		System.out.println(Sort.quickSort(peArray, peComparator, "random"));
		assertTrue(peArray[1].compareTo(peArray[2]) < 0);
		assertTrue(peArray[3].compareTo(peArray[2]) > 0);
		
	}
	
	@Ignore
	@Test
	public void testArraysSortPE() {
		Arrays.sort(peArray);
		assertTrue(peArray[0].compareTo(peArray[1]) < 0);
		assertTrue(peArray[3].compareTo(peArray[2]) > 0);
	}
	
//	@Ignore
//	@Test
//	public void testLineValid(){
//		String line1 = "LockheedMartian,36.03|NorthrupGreenman,16.56";
//		assertTrue(SortingClient.lineValid(line1));
//	}
	
//	@Ignore
//	@Test
//	public void testLineReadPairs(){
//		String line1 = "LockheedMartian,36.03|NorthrupGreenman,16.56";
//		Pair<Engine, Engine> testPE1 = SortingClient.readPairEngines(line1);
//		assertTrue(testPE1.compareTo(pe1) < 0);
//	}

}
