package edu.iastate.cs228.hw2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

/**
 * A client using the generic sorting methods in the Sort class to perform the
 * Engine sorting
 * 
 * @author Siyu Lin
 * 
 */
public class SortingClient {

	public static void main(String[] args) throws IOException {
		System.out
				.println("Welcome to Boris and Natasha's engine sorting client ");

		// Parse each file into an array
		// We assume that args[0] is EnginesSmall.txt, args[1] is
		// EnginesMedium.txt, args[2] is EnginesLarge.txt
		File smallEnginesFile = new File(args[0]);
		File mediumEnginesFile = new File(args[0]);
		File largeEnginesFile = new File(args[0]);

		Pair<Engine, Engine>[] pairSmall;
		Pair<Engine, Engine>[] pairMedium;
		Pair<Engine, Engine>[] pairLarge;

		if (checkFileFormat(smallEnginesFile)) {
			pairSmall = (Pair<Engine, Engine>[]) new Pair<?, ?>[countLines(smallEnginesFile)];
			createPairArray(pairSmall, smallEnginesFile);
		} else {
			return;
		}

		if (checkFileFormat(mediumEnginesFile)) {
			pairMedium = (Pair<Engine, Engine>[]) new Pair<?, ?>[countLines(mediumEnginesFile)];
			createPairArray(pairMedium, mediumEnginesFile);
		} else {
			return;
		}

		if (checkFileFormat(largeEnginesFile)) {
			pairLarge = (Pair<Engine, Engine>[]) new Pair<?, ?>[countLines(largeEnginesFile)];
			createPairArray(pairLarge, largeEnginesFile);
		} else {
			return;
		}

		// Normal Comparator by price
		Comparator<Pair<Engine, Engine>> peComparator = new Comparator<Pair<Engine, Engine>>() {

			@Override
			public int compare(Pair<Engine, Engine> o1, Pair<Engine, Engine> o2) {
				return o1.compareTo(o2);
			}
		};

		// Comparator by size
		Comparator<Pair<Engine, Engine>> peSizeComparator = new Comparator<Pair<Engine, Engine>>() {

			@Override
			public int compare(Pair<Engine, Engine> o1, Pair<Engine, Engine> o2) {
				if (o1.getFirst().getSize() > o2.getFirst().getSize()) {
					return 1;
				} else if (o1.getFirst().getSize() < o2.getFirst().getSize()) {
					return -1;
				} else {
				//If their first elements are equal, we compare their second elements
					if (o1.getSecond().compareTo(o2.getSecond()) > 0) {
						return 1;
					} else if (o1.getSecond().compareTo(o2.getSecond()) < 0) {
						return -1;
					} else {
						return 0;
					}
				 	
				}
			}
		};
		if (pairSmall != null && pairSmall != null && pairLarge != null) {
			System.out.println();
			System.out.println("Sort " + args[0]);
			generateSortedOutput(pairSmall, args[0], "Insertion", "Price",
					peComparator);
			generateSortedOutput(pairSmall, args[0], "Insertion", "Size",
					peSizeComparator);
			generateSortedOutput(pairSmall, args[0], "Selection", "Price",
					peComparator);
			generateSortedOutput(pairSmall, args[0], "Selection", "Size",
					peSizeComparator);
			generateSortedOutput(pairSmall, args[0], "QuickF", "Price",
					peComparator);
			generateSortedOutput(pairSmall, args[0], "QuickF", "Size",
					peSizeComparator);
			generateSortedOutput(pairSmall, args[0], "QuickM", "Price",
					peComparator);
			generateSortedOutput(pairSmall, args[0], "QuickM", "Size",
					peSizeComparator);
			System.out.println();
			System.out.println("Sort " + args[1]);
			generateSortedOutput(pairMedium, args[1], "Insertion", "Price",
					peComparator);
			generateSortedOutput(pairMedium, args[1], "Insertion", "Size",
					peSizeComparator);
			generateSortedOutput(pairMedium, args[1], "Selection", "Price",
					peComparator);
			generateSortedOutput(pairMedium, args[1], "Selection", "Size",
					peSizeComparator);
			generateSortedOutput(pairMedium, args[1], "QuickF", "Price",
					peComparator);
			generateSortedOutput(pairMedium, args[1], "QuickF", "Size",
					peSizeComparator);
			generateSortedOutput(pairMedium, args[1], "QuickM", "Price",
					peComparator);
			generateSortedOutput(pairMedium, args[1], "QuickM", "Size",
					peSizeComparator);
			System.out.println();
			System.out.println("Sort " + args[2]);
			generateSortedOutput(pairLarge, args[2], "Insertion", "Price",
					peComparator);
			generateSortedOutput(pairLarge, args[2], "Insertion", "Size",
					peSizeComparator);
			generateSortedOutput(pairLarge, args[2], "Selection", "Price",
					peComparator);
			generateSortedOutput(pairLarge, args[2], "Selection", "Size",
					peSizeComparator);
			generateSortedOutput(pairLarge, args[2], "QuickF", "Price",
					peComparator);
			generateSortedOutput(pairLarge, args[2], "QuickF", "Size",
					peSizeComparator);
			generateSortedOutput(pairLarge, args[2], "QuickM", "Price",
					peComparator);
			generateSortedOutput(pairLarge, args[2], "QuickM", "Size",
					peSizeComparator);
		}
	}

	/**
	 * Generate sorted output by given Pair array, input file name, sorting
	 * method, comparator name and comparators
	 * 
	 * @param pairArray
	 *            An pair array to be sorted
	 * @param inputFileName
	 *            The input file name of valid format
	 * @param sortMethod
	 *            The method user choose to perform
	 * @param comparartorName
	 *            The name of the comparator, Price or Size
	 * @param cp
	 *            The Comparator
	 * @throws FileNotFoundException
	 *             If output file is not found
	 */
	private static void generateSortedOutput(Pair<Engine, Engine>[] pairArray,
			String inputFileName, String sortMethod, String comparartorName,
			Comparator cp) throws FileNotFoundException {
		// Create output file
		PrintWriter outfile = null;
		String fileName = null;
		if (sortMethod.equals("Insertion") && comparartorName.equals("Price")) {
			fileName = inputFileName.replace(".txt", "InsertionSortPrice.txt");
		} else if (sortMethod.equals("Insertion")
				&& comparartorName.equals("Size")) {
			fileName = inputFileName.replace(".txt", "InsertionSortSize.txt");
		} else if (sortMethod.equals("Selection")
				&& comparartorName.equals("Price")) {
			fileName = inputFileName.replace(".txt", "SelectionSortPrice.txt");
		} else if (sortMethod.equals("Selection")
				&& comparartorName.equals("Size")) {
			fileName = inputFileName.replace(".txt", "SelectionSortSize.txt");
		} else if (sortMethod.equals("QuickF")
				&& comparartorName.equals("Price")) {
			fileName = inputFileName.replace(".txt", "QuickSortFistPrice.txt");
		} else if (sortMethod.equals("QuickF")
				&& comparartorName.equals("Size")) {
			fileName = inputFileName.replace(".txt", "QuickSortFirstSize.txt");
		} else if (sortMethod.equals("QuickM")
				&& comparartorName.equals("Price")) {
			fileName = inputFileName
					.replace(".txt", "QuickSortMedianPrice.txt");
		} else if (sortMethod.equals("QuickM")
				&& comparartorName.equals("Size")) {
			fileName = inputFileName.replace(".txt", "QuickSortMedianSize.txt");
		} else {
			throw new IllegalArgumentException(
					"You have not defined the method");
		}
		outfile = new PrintWriter(new File(fileName));

		// Create a temp array
		Pair<Engine, Engine>[] pairTemp = (Pair<Engine, Engine>[]) new Pair<?, ?>[pairArray.length];
		createTempArray(pairArray, pairTemp);

		// Print the sorted result to the out file
		printSortResult(outfile, pairTemp, sortMethod, comparartorName, cp);
	}

	/**
	 * Print the sorted result to an output file from a sorted array
	 * 
	 * @param outfile
	 *            The out file to be printed
	 * @param tempArray
	 *            The sorted array
	 * @param sortingMethod
	 *            The sorting method given by user
	 * @param comparatorName
	 *            The name of the comparator
	 * @param cp
	 *            The comparator it takes to perform sorting
	 */
	private static void printSortResult(PrintWriter outfile,
			Pair<Engine, Engine>[] tempArray, String sortingMethod,
			String comparatorName, Comparator cp) {
		long comparisons = 0;
		String firstLine = null;

		if (sortingMethod.equals("QuickF")) {
			if (comparatorName.equals("Price")) {
				comparisons = Sort.quickSort(tempArray, cp, "first");
				firstLine = "Quick sort first by price: " + comparisons;

			} else if (comparatorName.equals("Size")) {
				comparisons = Sort.quickSort(tempArray, cp, "first");
				firstLine = "Quick sort first by size: " + comparisons;
			}
		} else if (sortingMethod.equals("QuickM")) {
			if (comparatorName.equals("Price")) {
				comparisons = Sort.quickSort(tempArray, cp, "median");
				firstLine = "Quick sort Median by price: " + comparisons;
			} else if (comparatorName.equals("Size")) {
				comparisons = Sort.quickSort(tempArray, cp, "median");
				firstLine = "Quick sort Median by size: " + comparisons;
			}

		} else if (sortingMethod.equals("Selection")) {
			if (comparatorName.equals("Price")) {
				comparisons = Sort.selectionSort(tempArray, cp);
				firstLine = "Selection sort by price: " + comparisons;

			} else if (comparatorName.equals("Size")) {
				comparisons = Sort.selectionSort(tempArray, cp);
				firstLine = "Selection sort by size: " + comparisons;
			}

		} else if (sortingMethod.equals("Insertion")) {
			if (comparatorName.equals("Price")) {
				comparisons = Sort.insertionSort(tempArray, cp);
				firstLine = "Insertion sort by price: " + comparisons;

			} else if (comparatorName.equals("Size")) {
				comparisons = Sort.insertionSort(tempArray, cp);
				firstLine = "Insertion sort by size: " + comparisons;
			}
		} else {
			throw new IllegalArgumentException(
					"You have not defined the method");
		}
		System.out.println(firstLine);
		outfile.println(firstLine);
		printSortedPairs(tempArray, outfile);
		outfile.close();

	}

	/**
	 * Create a copy of a given array
	 * 
	 * @param pair
	 *            The array to be copied
	 * @param tempArray
	 *            The resulted array identical to the previous array
	 */
	private static void createTempArray(Pair<Engine, Engine>[] pair,
			Pair<Engine, Engine>[] tempArray) {
		if (tempArray.length == pair.length) {
			System.arraycopy(pair, 0, tempArray, 0, pair.length);
		}
	}

	/**
	 * Print sorted pairs to a file
	 * 
	 * @param sortedPairs
	 *            The sorted Engine Pairs
	 * @param outFile
	 *            The output file to be printed
	 */
	private static void printSortedPairs(Pair<Engine, Engine>[] sortedPairs,
			PrintWriter outFile) {

		for (Pair<Engine, Engine> pairEngine : sortedPairs) {
			String firstEngineStr = pairEngine.getFirst().getBrandName() + ","
					+ pairEngine.getFirst().getSize() + ","
					+ pairEngine.getFirst().getPrice();
			String secondEngineStr = pairEngine.getSecond().getBrandName()
					+ "," + pairEngine.getSecond().getSize() + ","
					+ pairEngine.getSecond().getPrice();
			outFile.println(firstEngineStr + "|" + secondEngineStr);
		}
		outFile.close();
	}

	/**
	 * Test if the line is valid
	 * 
	 * @param line
	 *            The line of a given file
	 * @return false if the line is not valid according to the spec
	 */
	private static boolean lineValid(String line) {
		ArrayList<String> pairs = extractPairsString(line);

		if (pairs.equals(null) || pairs.size() != 2) {
			return false;
		}

		for (String eng : pairs) {
			ArrayList<String> engine = extractEngineString(eng);

			if (engine.equals(null) || engine.size() != 2) {
				return false;
			}

			try {
				double size = Double.parseDouble(engine.get(1));
			} catch (NumberFormatException e) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Extract the brand name and size of an engine from a given string
	 * 
	 * @param eng
	 *            A string delimited by comma
	 * @return An ArrayList<String> from the given string delimited by comma
	 */
	private static ArrayList<String> extractEngineString(String eng) {
		Scanner pairScanner = new Scanner(eng);
		pairScanner.useDelimiter(",");
		ArrayList<String> engine = new ArrayList<String>();
		while (pairScanner.hasNext()) {
			engine.add(pairScanner.next().trim());
		}
		return engine;
	}

	/**
	 * Extract the pair string from the line
	 * 
	 * @param line
	 *            A line delimited by bar
	 * @return An ArrayList<String> from the given line delimited by bard
	 */
	private static ArrayList<String> extractPairsString(String line) {
		Scanner lineScanner = new Scanner(line);
		lineScanner.useDelimiter("\\|");
		ArrayList<String> pairs = new ArrayList<String>();
		while (lineScanner.hasNext()) {
			pairs.add(lineScanner.next().trim());
		}
		return pairs;
	}

	/**
	 * Get the Pair<Engine, Engine> from a given line. The program will be
	 * terminated if the line is not valid
	 * 
	 * @param line
	 *            A line consists of a pair of engines
	 * @return A Pair<Engine, Engine>
	 */
	private static Pair<Engine, Engine> readPairEngines(String line) {
		if (lineValid(line)) {
			ArrayList<String> pairs = extractPairsString(line);
			Engine[] enginePair = new Engine[2];

			for (int i = 0; i < pairs.size(); i++) {
				ArrayList<String> engine = extractEngineString(pairs.get(i));

				String name = engine.get(0);
				double size = Double.parseDouble(engine.get(1));

				enginePair[i] = new Engine(size, name);
			}

			return new Pair<Engine, Engine>(enginePair[0], enginePair[1]);
		} else {
			System.out.println("The format is not correct");
			return null;

		}
	}

	/**
	 * Count line numbers of a given file
	 * 
	 * @param file
	 *            A text file
	 * @return the line numbers of the file
	 * @throws FileNotFoundException
	 *             if the file can not be found
	 */
	private static int countLines(File file) throws FileNotFoundException {
		Scanner lineScanner = new Scanner(file);
		int lineNumber = 0;
		// while there are more lines...
		while (lineScanner.hasNextLine()) {
			lineScanner.next();
			lineNumber++;
		}
		// close the file
		lineScanner.close();

		return lineNumber;
	}

	/**
	 * Create a Pair<Engine, Engine> array from a given file
	 * 
	 * @param pairArray
	 *            An empty Pair<Engine, Engine> array
	 * @param file
	 *            The file to be read
	 * @throws FileNotFoundException
	 *             The file can not be found
	 */
	private static void createPairArray(Pair<Engine, Engine>[] pairArray,
			File file) throws FileNotFoundException {
		Scanner lineScanner = new Scanner(file);
		if (checkFileFormat(file)) {
			int lineNumber = 0;
			// while there are more lines...
			while (lineScanner.hasNextLine()) {
				pairArray[lineNumber] = readPairEngines(lineScanner.next()
						.trim());
				lineNumber++;
			}
			// close the file
			lineScanner.close();
		} else {
			System.out.println("The format is not correct");
			// System.exit(0);
			return;
		}
	}

	/**
	 * Check if the file's format is correct
	 * 
	 * @param file
	 *            The file to be read
	 * @return false if the file is null or any line is not valid
	 * @throws FileNotFoundException
	 *             if the file can not be found
	 */
	private static boolean checkFileFormat(File file)
			throws FileNotFoundException {
		if (file.equals(null)) {
			return false;
		} else {
			Scanner lineScanner = new Scanner(file);
			// while there are more lines...
			while (lineScanner.hasNextLine()) {
				if (!lineValid(lineScanner.next())) {
					return false;
				}
			}
			// close the file
			lineScanner.close();
		}
		return true;
	}
}
