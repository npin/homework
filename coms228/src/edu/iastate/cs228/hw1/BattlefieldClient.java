package edu.iastate.cs228.hw1;

import java.util.Scanner;
import java.util.InputMismatchException;

/**
 * A client to play a game of MilitaryTransport's simulation in the Battlefield
 * 
 * @author Siyu Lin
 * 
 */
public class BattlefieldClient {
	public static <T extends MilitaryTransport> void main(String[] args)
			throws BattlefieldException {
		Battlefield<T> battlefield = null;
		playGame(battlefield);

	}

	/**
	 * Play a game in a Battlefield
	 * 
	 * @param bf
	 *            The Battlefield which we will play a game in
	 * @throws BattlefieldException
	 *             if the Battlfield created is not valid
	 */
	private static <T extends MilitaryTransport> void playGame(Battlefield<T> bf)
			throws BattlefieldException {
		Scanner userInput = new Scanner(System.in);
		bf = enterSizeofBattlefield(bf);
		while (true) {
			System.out
					.println("Please enter a MilitaryTransport in forms of type, name, initial defense or start the game (\"s\", Case sensitive!)");
			String text = userInput.next();
			if (startGame(text)) {
				break;
			}
			addMilitatyTransport(bf, text);
		}
		simulateBattleField(bf);
		System.out
				.println("Do you want to simulate again? Y/N (Case sensitive)");
		String text = userInput.next();
		if (text.equals("Y")) {
			playGame(bf);
		} else if (text.equals("N")) {
			userInput.close();
		}
	}

	/**
	 * Battlefield starts one round of simulation in the client and then a list
	 * of MilitaryTransports in sorted order will be printed out
	 * 
	 * @param bf
	 *            Battlefield in the client
	 * @throws BattlefieldException
	 *             If there are not enough MiltaryTransports in the Battlefield
	 */
	private static <T extends MilitaryTransport> void simulateBattleField(
			Battlefield<T> bf) throws BattlefieldException {
		try {
			if (bf == null) {
				throw new BattlefieldException("The Battlefield is empty");
			}

			bf.simulate();
			String[] transports = bf.listTransportsSorted();
			if (transports != null) {
				for (int i = 0; i < transports.length; i++) {
					System.out.println(transports[i]);
				}
			} else {
				System.out.println("The Battlefield is empty now");
			}
		} catch (BattlefieldException e) {
			System.out
					.println("Please restart the game and then add more to do the simulation");
		}

	}

	/**
	 * Add a MilitaryTransport to the Battlefield according to the user's input
	 * 
	 * @param bf
	 *            The Battlefield in the client
	 * @param text
	 *            The user's input
	 * @throws BattlefieldException
	 *             If the input is not valid or the format is not correct
	 */
	private static <T extends MilitaryTransport> void addMilitatyTransport(
			Battlefield<T> bf, String text) throws BattlefieldException {
		if (isFormatCorrect(text) && isTypeOfTransportValid(text)) {
			T mt = null;
			mt = (T) parseInputToMilitaryTransport(text, mt);

			if (mt == null) {
				throw new BattlefieldException(
						"The MilitaryTransport entered is not valid");
			}

			addWithinCapacity(mt, bf);
		} else {
			System.out
					.println("The format or type is not correct, please enter again");
			Scanner sc = new Scanner(System.in);
			String nextInput = sc.next();
			addMilitatyTransport(bf, nextInput);
			return;
		}
	}

	/**
	 * Asking user to set the capacity of the Battlefield
	 * 
	 * @param bf
	 *            The originally null Battlefield in the client
	 * @return The Battlefield after setting the capacity
	 */
	private static <T extends MilitaryTransport> Battlefield<T> enterSizeofBattlefield(
			Battlefield<T> bf) {
		System.out.println("Please enter the size of the battle field");
		Scanner input = new Scanner(System.in);
		try {
			int size = input.nextInt();
			bf = new Battlefield<T>(size);
			return bf;
		}

		catch (InputMismatchException e) {
			System.out.println("Please enter a number");
			enterSizeofBattlefield(bf);
		}

		catch (BattlefieldException e) {
			System.out.println("Please try to enter another number");
			enterSizeofBattlefield(bf);
		}
		return bf;
	}

	/**
	 * Parse the user's input to a MilitaryTransport
	 * 
	 * @param <T>
	 *            Extends MilitaryTransport Class
	 * @param input
	 *            User's input
	 * @param mt
	 *            Originally null MilitaryTransport will be altered after
	 *            parsing
	 * @return
	 * @throws BattlefieldException
	 *             If the defense is less than 0, a BattlefieldException will be
	 *             thrown
	 */
	private static <T extends MilitaryTransport> MilitaryTransport parseInputToMilitaryTransport(
			String input, T mt) throws BattlefieldException {
		String[] mtAttributes = new String[3];
		parseInputToArray(input, mtAttributes);

		try {
			int defense = Integer.parseInt(mtAttributes[2]);

			if (defense < 0) {
				System.out.print("The defense is not valid");
				return mt;
			}

			if (mtAttributes[0].equals("ArmoredVehicle")) {
				mt = (T) new ArmoredVehicle(mtAttributes[1], defense);
				return mt;
			} else if (mtAttributes[0].equals("Tank")) {
				mt = (T) new Tank(mtAttributes[1], defense);
				return mt;
			} else if (mtAttributes[0].equals("Jeep")) {
				mt = (T) new Jeep(mtAttributes[1], defense);
				return mt;
			} else {
				return mt;
			}
		} catch (NumberFormatException e) {
			return mt;
		}

	}

	/**
	 * Parse user's input using delimiter of "," to an array of length 3
	 * 
	 * @param input
	 *            The input by the user
	 * @param targetArray
	 *            After parsing input, the null array length of 3 is altered
	 * 
	 */
	private static void parseInputToArray(String input, String[] targetArray) {
		String[] arrayOfInputs = input.split(",");

		if (arrayOfInputs.length != targetArray.length) {
			return;
		} else {
			for (int i = 0; i < targetArray.length; i++) {
				targetArray[i] = arrayOfInputs[i].trim();
			}
		}

	}

	/**
	 * Test the type of the MilitaryTransport is valid
	 * 
	 * @param input
	 *            The input by the user
	 * @return true if the the type of the MilitaryTransport is valid, i.e,
	 *         ArmoredVehicle or Jeep or Tank
	 */
	private static boolean isTypeOfTransportValid(String input) {
		String[] inputs = new String[3];
		parseInputToArray(input, inputs);
		if (inputs[0].equals("ArmoredVehicle") || inputs[0].equals("Jeep")
				|| inputs[0].equals("Tank")) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Test the user's input for the MilitaryTransport is correct of the
	 * following form, typeOfMilitaryTransport, name, defense
	 * 
	 * @param input
	 *            The input by the user
	 * @return true if the format is correct
	 */
	private static boolean isFormatCorrect(String input) {
		String[] inputs = new String[3];
		parseInputToArray(input, inputs);

		for (String e : inputs) {
			// If comma cannot do as delimiter, some element may be null
			if (e == null) {
				return false;
			}
		}

		try {
			int defense = Integer.parseInt(inputs[2]);

			// If defense is negative, the format is not correct
			if (defense < 0) {
				return false;
			}
		} catch (NumberFormatException e) {
			return false;
		}

		return true;
	}

	/**
	 * The game can only start when the input is lower case s
	 * 
	 * @param input
	 * @return
	 */
	private static boolean startGame(String input) {
		if (input.equals("s")) {
			return true;
		}
		return false;
	}

	/**
	 * The method is to add a MilitaryTransport to a Battlefield
	 * 
	 * @param mt
	 *            A MilitaryTransport to be added
	 * @param bf
	 *            The Battlefield in the client
	 * @throws BattlefieldException
	 *             If the name is duplicated
	 */
	private static <T extends MilitaryTransport> void addWithinCapacity(T mt,
			Battlefield<T> bf) throws BattlefieldException {
		if (bf.listTransportsSorted() != null) {
			int current = bf.listTransportsSorted().length;

			// If the capacity is reached, the player will have to start the
			// game
			if (bf.getCapacity() == current) {
				System.out.println("Please start the game!");
				return;
			}
		}
		try {
			bf.add(mt);
		} catch (BattlefieldException e) {
			System.out.println("Please enter another name");
		}

	}
}
