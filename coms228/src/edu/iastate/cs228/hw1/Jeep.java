package edu.iastate.cs228.hw1;

/**
 * Jeep is a kind of MilitaryTransport
 * 
 * @author Siyu Lin
 * 
 */
public class Jeep extends MilitaryTransport {

	/**
	 * Constructor for Jeep
	 * 
	 * @param name
	 *            The name of Jeep
	 * @param defense
	 *            The defense of Jeep, a positive integer
	 * @throws BattlefieldException
	 *             If the defense is negative, a BattlefieldException will be
	 *             thrown
	 */
	public Jeep(String name, int defense) {
		super(name, defense);
	}

	/**
	 * When Jeep interact with other MilitaryTransports, it only makes noise
	 * because Jeep does not have weapons
	 */
	@Override
	public void interact(MilitaryTransport t) {
		makeNoise();
	}

	/**
	 * The way Jeep makes noise
	 */
	@Override
	public void makeNoise() {
		System.out.println("Beep");

	}

}
