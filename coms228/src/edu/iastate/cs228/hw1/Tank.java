package edu.iastate.cs228.hw1;

/**
 * Tank is a kind of ArmoredVehicle
 * 
 * @author Siyu Lin
 * 
 */
public class Tank extends ArmoredVehicle {

	/**
	 * Constructor for Tank
	 * 
	 * @param name
	 *            The name of Tank
	 * @param defense
	 *            The defense of Tank, a positive integer
	 * @throws BattlefieldException
	 *             If the defense is negative, a BattlefieldException will be
	 *             thrown
	 */
	public Tank(String name, int defense) {
		super(name, defense);
	}

	/**
	 * The way Tank makes noise
	 */
	@Override
	public void makeNoise() {
		System.out.println("Boom");
	}

	/**
	 * The way Tank interacts with other MilitaryTransports. When the other one
	 * is Jeep, it will make noise and add the Jeep's defense and then set the
	 * Jeep's defense to 0. When the other one is Tank, it will only make noise.
	 * When the other one is a normal ArmoredVehicle other than Jeep or Tank,
	 * the attack, itself, will lose 10% of its defense and the defender, the
	 * other one will lose 50% of its defense.
	 */
	@Override
	public void interact(MilitaryTransport other) {
		if (other instanceof Jeep) {
			makeNoise();
			int jeepDefense = other.getDefense();
			this.setDefense(getDefense() + jeepDefense);
			other.setDefense(0);
		} else if (other instanceof Tank) {
			makeNoise();
		} else if (other instanceof ArmoredVehicle) {
			this.setDefense((int) (getDefense() * 0.9));
			other.setDefense((int) (other.getDefense() * 0.5));
		}
	}

}
