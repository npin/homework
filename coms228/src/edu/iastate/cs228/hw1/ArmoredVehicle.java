package edu.iastate.cs228.hw1;

/**
 * ArmoredVehicle is a kind of MilitaryTransport
 * 
 * @author Siyu Lin
 * 
 */
public class ArmoredVehicle extends MilitaryTransport {

	/**
	 * Constructor for ArmoredVehicle
	 * 
	 * @param name
	 *            The name of ArmoredVehicle
	 * @param defense
	 *            The defense of ArmoredVehicle, a positive integer
	 * @throws BattlefieldException
	 *             If the defense is negative, a BattlefieldException will be
	 *             thrown
	 */
	public ArmoredVehicle(String name, int defense)  {
		super(name, defense);
	}

	/**
	 * The way ArmoredVehicle interacts with other MilitaryTransports. When the
	 * other one is Jeep, it will make noise and add up the Jeep's defense and
	 * then set the Jeep's defense to 0. When the other one is ArmoredVehicle
	 * other than Jeep or Tank, it will only make noise. When the other one is a
	 * Tank , the attacker, itself, will lose 10% of its defense, and the
	 * defender, the other one will lose 20% of its defense.
	 */
	@Override
	public void interact(MilitaryTransport other) {
		if (other instanceof Jeep) {
			makeNoise();
			int jeepDefense = other.getDefense();
			this.setDefense(getDefense() + jeepDefense);
			other.setDefense(0);
		} else if (other instanceof Tank) {
			this.setDefense((int) (getDefense() * 0.9));
			other.setDefense((int) (other.getDefense() * 0.8));
		} else if (other instanceof ArmoredVehicle) {
			makeNoise();
		}
	}

	/**
	 * The way ArmoredVehicle makes noise
	 */
	@Override
	public void makeNoise() {
		System.out.println("Pew pew pew");
	}

}
