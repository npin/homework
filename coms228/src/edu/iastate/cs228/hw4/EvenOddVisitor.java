package edu.iastate.cs228.hw4;

/**
 * A Visitor which counts the number of even numbers and the number of odd
 * numbers of a BST
 * 
 * @author Siyu Lin
 * 
 * @param <E>
 */
public class EvenOddVisitor<E extends Comparable<? super E>> implements
		Visitor<E> {
	/**
	 * The counter for even numbers
	 */
	private int numEvens;
	/**
	 * The counter for odd numbers
	 */
	private int numOdds;

	public EvenOddVisitor() {
		numEvens = 0;
		numOdds = 0;
	}

	/**
	 * Visit the node of the BST
	 */
	@Override
	public void visit(BSTNode<E> node) {
		// if(node.data instanceof Integer){
		try {
			int number = Integer.parseInt((String) node.data);
			if (number % 2 == 0) {
				numEvens++;
			} else {
				numOdds++;
			}
		} catch (NumberFormatException e) {
			System.out.println("The string is not valid");
		}

		// }
	}

	/**
	 * Return the number of odd numbers
	 * 
	 * @return the number of even numbers
	 */
	public String getNumEvens() {
		return "" + numEvens;
	}

	/**
	 * Return the number of odd numbers
	 * 
	 * @return
	 */
	public String getNumOdds() {
		return "" + numOdds;
	}

}
