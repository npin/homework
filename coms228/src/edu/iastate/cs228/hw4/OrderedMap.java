package edu.iastate.cs228.hw4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.NoSuchElementException;
import java.util.Stack;
import java.util.Iterator;
import java.util.Map;

/**
 * An Ordered Map in which keys are put in a BST
 * 
 * @author Siyu Lin
 * 
 * @param <O>
 *            type of the ordering key
 * @param <M>
 *            type of the map value
 */
public class OrderedMap<O extends Comparable<? super O>, M> implements
		OrderedMapInterface<O, M> {
	/**
	 * The back store of the keys and their associated values
	 */
	private Map<O, M> map;
	/**
	 * The root of the BST
	 */
	private BSTNode<O> root;
	/**
	 * The size of the map
	 */
	private int size;

	public OrderedMap() {
		map = new HashMap<O, M>();
	}

	/**
	 * removes all items from the OrderedMap
	 */
	@Override
	public void clear() {
		map.clear();
		size = 0;
		root = null;
	}

	/**
	 * 
	 * @param orderingKey
	 * @return the map value associated with this orderingKey, or null if none
	 *         exists
	 */
	@Override
	public M get(Object orderingKey) {
		return map.get(orderingKey);
	}

	/**
	 * associates the given orderingKey with the given mapValue and adds the
	 * orderingKey to the BST
	 * 
	 * @param orderingKey
	 *            An ordering key in the BST and can not be null in order to be
	 *            compared to other keys
	 * @param mapValue
	 *            The map values associated with the ordering key
	 */
	@Override
	public void put(O orderingKey, M mapValue) {
		if (!containsOrderingKey(orderingKey)) {
			add(orderingKey);
			size++;
		}
		if (orderingKey != null) {
			map.put(orderingKey, mapValue);
		}
	}

	/**
	 * 
	 * @return the total number of O,M pairs in this OrderedMap
	 */
	@Override
	public int size() {
		return map.size();
	}

	/**
	 * remove any mapping associated with this orderingKey and removes the
	 * orderingKey from the BST
	 * 
	 * @param orderingKey
	 * @return true if the remove was successful, false otherwise
	 */
	@Override
	public boolean remove(Object orderingKey) {
		if (map.remove(orderingKey) != null) {
			try {
				removeKey(orderingKey);
				size--;
				return true;
			} catch (NoSuchElementException e) {
				return false;
			}
		}
		return false;
	}

	/**
	 * Remove a key from the the BST
	 * 
	 * @param orderingKey
	 *            The key to be removed
	 * @return the node removed
	 */
	private BSTNode<O> removeKey(Object orderingKey) {
		BSTNode<O> target = searchTarget(orderingKey);
		if (target != null) {
			return removeNode(target);
		}
		return null;
	}

	/**
	 * Search the key in the Binary Search Tree and then return its node
	 * Precondition the node is not null
	 * 
	 * @param orderingKey
	 *            The targeted key
	 * @return a node containing the key
	 */
	private BSTNode<O> searchTarget(Object orderingKey) {
		if (orderingKey == null) {
			throw new NoSuchElementException();
		}
		BSTNode<O> node = root;
		// Credits to the TreeMap by Sun
		while (node != null && orderingKey.getClass() == (node.data).getClass()) {
			O target = (O) orderingKey;
			int cmp = target.compareTo(node.data);
			// When the input is less than the root, search the left
			// subtree
			if (cmp < 0) {
				if (node.leftChild != null)
					node = node.leftChild;
				else {
					return null;
				}
			}
			// When the input is greater than the root, search the right
			// subtree
			else if (cmp > 0) {
				if (node.rightChild != null) {
					node = node.rightChild;
				} else {
					return null;
				}
			} else {
				return node;
			}
		}
		return null;
	}

	/**
	 * Remove the node from the BST. Precondition: the node is in the BST
	 * 
	 * @param node
	 *            The node to be removed
	 * @return the new node which replaces the removed node
	 */
	private BSTNode<O> removeNode(BSTNode<O> node) {
		// Credits to the textbook
		if (node == null || searchTarget(node.data) == null) {
			throw new NoSuchElementException();
		}
		// When the node has two children
		if (node.leftChild != null && node.rightChild != null) {
			BSTNode<O> replacement = successor(node);
			node.data = replacement.data;
			// Move the successor's left child or right child to its original
			// position
			BSTNode<O> newChild = replacement.leftChild == null ? replacement.rightChild
					: replacement.leftChild;
			if (replacement == replacement.parent.leftChild) {
				replacement.parent.leftChild = newChild;
			} else {
				replacement.parent.rightChild = newChild;
			}

			if (newChild != null && newChild.parent != null) {
				newChild.parent = replacement.parent;
			}

		} else {
			BSTNode<O> newChild = (node.leftChild == null) ? node.rightChild
					: node.leftChild;
			if (node.parent != null) {
				if (node == node.parent.leftChild) {
					node.parent.leftChild = newChild;
				} else {
					node.parent.rightChild = newChild;
				}
			}
			if (newChild != null) {
				newChild.parent = node.parent;
			}
			if (node == root) {
				root = newChild;
			}
			node = newChild;
		}
		return node;
	}

	/**
	 * 
	 * @return an iterator that iterates through the ordering keys in-order
	 */
	@Override
	public Iterator<O> keyIterator() {
		return new OrderedMapKeyIterator();
	}

	/**
	 * 
	 * @param orderingKey
	 * @return true if the orderingKey is in the OrderedMap. False otherwise.
	 */
	@Override
	public boolean containsOrderingKey(Object orderingKey) {
		return map.containsKey(orderingKey);
	}

	/**
	 * determines whether this OrderedMap contains an ordering key that maps to
	 * the input mapValue
	 * 
	 * @param mapValue
	 * @return true if there is some orderingKey that maps to mapValue, false
	 *         otherwise
	 */
	@Override
	public boolean containsMapValue(Object mapValue) {
		return map.containsValue(mapValue);
	}

	/**
	 * returns as an ArrayList all orderingKeys, in ascending order
	 * 
	 * @return an array of orderingKeys
	 */
	@Override
	public ArrayList<O> keysInAscendingOrder() {
		Iterator<O> iter = new OrderedMapKeyIterator();
		ArrayList<O> keys = new ArrayList<O>();
		while (iter.hasNext()) {
			keys.add(iter.next());
		}
		return keys;
	}

	/**
	 * 
	 * @return an ArrayList of all map values, in ascending order (as ordered by
	 *         ordering keys)
	 */
	@Override
	public ArrayList<M> valuesInAscendingOrder() {
		ArrayList<O> keys = keysInAscendingOrder();
		ArrayList<M> values = new ArrayList<M>();
		for (O k : keys) {
			values.add(get(k));
		}
		return values;
	}

	/**
	 * ceiling function for ordering keys returns least orderingKey > the input,
	 * or null if there is no such ordering key
	 * 
	 * @param orderingKey
	 * @return the least object of type O that is greater than the input
	 */
	@Override
	public O ceiling(O orderingKey) {
		// Credits to the TreeMap by Sun
		if (orderingKey == null
				|| orderingKey.getClass() != root.data.getClass()) {
			// throw new IllegalArgumentException();
			return null;
		}
		O target = (O) orderingKey;
		// The keys are in ascending order
		ArrayList<O> keys = keysInAscendingOrder();
		for (O key : keys) {
			int cmp = target.compareTo(key);
			// Only if the key is greater than the input
			if (cmp < 0) {
				return key;
			}
		}
		return null;
	}

	/**
	 * 
	 * @param fromKey
	 * @param toKey
	 * @return an orderedMap that takes all orderingKeys and map values >=
	 *         fromkey and <= toKey (inclusive) or returns null if either
	 *         fromKey or toKey does not exist
	 */
	@Override
	public OrderedMapInterface<O, M> subMap(O fromKey, O toKey) {
		O highest = keysInAscendingOrder().get(size - 1);
		O lowest = keysInAscendingOrder().get(0);
		if (fromKey.compareTo(toKey) > 0 || fromKey.compareTo(highest) > 0
				|| toKey.compareTo(lowest) < 0) {
			return null;
		}
		// Find all the keys that are greater or equal to the fromKey and less
		// or equal to the toKey
		ArrayList<O> allKeys = keysInAscendingOrder();
		ArrayList<O> retKeys = new ArrayList<O>();
		for (O key : allKeys) {
			boolean inRange = key.compareTo(fromKey) >= 0
					&& key.compareTo(toKey) <= 0;
			if (inRange) {
				retKeys.add(key);
			}
		}
		if (retKeys.size() != 0) {
			OrderedMap<O, M> sub = new OrderedMap();
			for (int i = 0; i < retKeys.size(); i++) {
				O orderingKey = retKeys.get(i);
				sub.put(orderingKey, this.get(orderingKey));
			}
			return sub;
		} else {
			return null;
		}
	}

	/**
	 * In Order Traversal for a Visitor Class
	 * 
	 * @param v
	 *            The visitor for the BST
	 */
	public void inOrderTraversal(Visitor<O> v) {
		// Credits to Geeks for geeks
		// 1) Create an empty stack S.
		Stack<BSTNode> tempStack = new Stack();
		// 2) Initialize current node as root
		BSTNode<O> current = root;
		boolean done = false;
		// 3) Push the current node to S and set current = current->left until
		// current is NULL
		while (current != null) {
			tempStack.push(current);
			current = current.leftChild;
		}
		// 5) If current is NULL and stack is empty then we are done.
		while (!done) {
			if (current == null && tempStack.isEmpty()) {
				done = true;
			}
			if (current != null) {
				tempStack.push(current);
				current = current.leftChild;
			}
			// 4) If current is NULL and stack is not empty then
			// a) Pop the top item from stack.
			// b) Print the popped item, set current = current->right
			// c) Go to step 3.
			else {
				if (!tempStack.isEmpty()) {
					current = tempStack.pop();
					// Visit the node
					v.visit(current);
					current = current.rightChild;
				}
			}
		}

	}

	/**
	 * Add an ordering key to the BST
	 * 
	 * @param orderingKey
	 *            the key to be added to the BST
	 */
	private void add(O orderingKey) {
		if (orderingKey == null) {
			throw new NullPointerException();
		} else if (root == null) {
			// When the tree is empty
			root = new BSTNode(orderingKey);
		} else {
			if (!containsOrderingKey(orderingKey)) {
				BSTNode<O> p = root;
				while (p != null) {
					int cmp = orderingKey.compareTo(p.data);
					if (cmp < 0) {
						if (p.leftChild != null) {
							p = p.leftChild;
						} else {
							BSTNode<O> child = new BSTNode(orderingKey);
							p.leftChild = child;
							child.parent = p;
							return;
						}
					} else if (cmp > 0) {
						if (p.rightChild != null) {
							p = p.rightChild;
						} else {
							BSTNode<O> child = new BSTNode(orderingKey);
							p.rightChild = child;
							child.parent = p;
							return;
						}
					}
				}
			}
		}
	}

	/**
	 * Find a successor of a node, Precondition: the node has two children
	 * 
	 * @param node
	 *            the node in the BST
	 * @return the successor of the node
	 */
	// Credits to The Text Book
	private BSTNode<O> successor(BSTNode<O> node) {
		if (node == null || node.rightChild == null) {
			return null;
		}
		node = node.rightChild;
		while (node.leftChild != null) {
			node = node.leftChild;
		}
		return node;
	}

	/**
	 * An iterator over the BST
	 */
	private class OrderedMapKeyIterator implements Iterator<O> {

		/**
		 * The node to be pushed to the stack
		 */
		private BSTNode<O> current;
		/**
		 * The back store of nodes
		 */
		private Stack<BSTNode<O>> stack;
		/**
		 * True if you can remove the node returned by next()
		 */
		private boolean canRemove;
		/**
		 * The value returned by next()
		 */
		private O previous;

		public OrderedMapKeyIterator() {
			stack = new Stack<BSTNode<O>>();
			current = root;
			while (current != null) {
				stack.push(current);
				current = current.leftChild;
			}
			canRemove = false;
			previous = null;
		}

		/**
		 * Returns true if the iteration has more elements.
		 */
		@Override
		public boolean hasNext() {
			return !stack.isEmpty();
		}

		/**
		 * Returns the next element in the iteration.
		 */
		@Override
		public O next() {
			if (stack.isEmpty()) {
				throw new NoSuchElementException();
			}

			// 4) If current is NULL and stack is not empty then
			// a) Pop the top item from stack.
			// b) Print the popped item, set current = current->right
			// c) Go to step 3.

			current = stack.pop();
			// Visit the node
			O value = current.data;
			current = current.rightChild;
			while (current != null) {
				stack.push(current);
				current = current.leftChild;
			}
			canRemove = true;
			previous = value;
			return value;
		}

		/**
		 * Removes the node of previous.
		 */
		@Override
		public void remove() {
			if (!canRemove) {
				throw new IllegalStateException();
			}
			if (previous != null)
				OrderedMap.this.remove(previous);
			canRemove = false;
		}

	}
}
