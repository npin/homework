package lab2;
/**
 * Model of an atom for use in quality control simulations
 * @author Siyu Lin
 */

public class Atom 
{
	/**
	 * The number of protons of the atom
	 */
	private int protons; 
	/**
	 * The number of neurons of the atom
	 */
	private int neurons;
	/**
	 * The number of electrons of the atom
	 */
	private int electrons;
	// Constructors initialize the instance variables 
	
	/**
	 * Contructs an an atom with given parameters
	 * @param givenProtons
	 * 	The protons for the atom
	 * @param givenNeurons
	 * 	The neurons for the atom
	 * @param givenElectrons
	 * 	The electrons for the atom
	 */
	public Atom(int givenProtons, int givenNeurons, int givenElectrons)
	{
		protons = givenProtons;
		neurons = givenNeurons;
		electrons = givenElectrons;
	}
	// Methods define the operations of an atom
	
	/**
	 * Return the mass of the atom
	 * @return the mass of the atom
	 */
	public int getAtomicMass() 
	{
		return protons + neurons;
	}
	/**
	 * Return the charge of the atom
	 * @return the charge of the atom
	 */
	public int getAtomicCharge()
	{
		return protons-electrons;
	}
	/**
	 * The atom decayed
	 */
	public void decay()
	{
		protons = protons-2;
		neurons = neurons-2;
	}
}
